QT       += core gui network charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += \
	  $${PWD}/src \
	  $${PWD}/../src

SOURCES += \
	$${PWD}/src/mainwindow.cpp \
	$${PWD}/src/networkactions.cpp \
	$${PWD}/src/channelsform.cpp \
	$${PWD}/src/setupdialog.cpp \
	$${PWD}/src/linechart.cpp \
	$${PWD}/src/signalview.cpp \
	$${PWD}/src/paramterscalculator.cpp \
	main.cpp


HEADERS += \
	$${PWD}/src/mainwindow.hpp \
	$${PWD}/src/networkactions.hpp \
	$${PWD}/src/channelsform.hpp \
	$${PWD}/src/setupdialog.hpp \
	$${PWD}/src/linechart.hpp \
	$${PWD}/src/signalview.hpp \
	$${PWD}/src/paramterscalculator.hpp

FORMS += \
	$${PWD}/src/mainwindow.ui \
	$${PWD}/src/channelsform.ui \
	$${PWD}/src/signalview.ui \
	$${PWD}/src/setupdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
	demo-resource.qrc
