﻿#include "channelsform.hpp"
#include <QPainter>

ChannelsForm::ChannelsForm(QWidget* parent) :
	QWidget(parent)
{
	ui.setupUi(this);
	initIcons();
	resetChannelsState();
}

QIcon makePixmap(QSize size, QColor color)
{
	QPixmap pixmap(size);
	QPainter p(&pixmap);
	p.setRenderHint(QPainter::RenderHint::Antialiasing);

	p.setBackground(QBrush(Qt::GlobalColor::transparent));
	p.setBrush(QBrush(color));
	QRect r = QRect(QPoint(0, 0), size);
	int radius = size.width() / 2;
	radius -= 2;
	p.fillRect(r, Qt::GlobalColor::lightGray);

	p.drawEllipse(r.center(), radius, radius);
	return QIcon(pixmap);
}

void ChannelsForm::initIcons()
{
	constexpr QSize ICON_SIZE(32, 32);
	icons[size_t(ChannelState::Unknown)] = makePixmap(ICON_SIZE, Qt::gray);
	icons[size_t(ChannelState::Bad)] = makePixmap(ICON_SIZE, Qt::red);
	icons[size_t(ChannelState::Good)] = makePixmap(ICON_SIZE, Qt::darkGreen);
}

void ChannelsForm::resetChannelsState()
{
	for (size_t index = 0; index < CHANNEL_COUNT; index++)
	{
		setChannelState(index, ChannelState::Unknown);
	}
}

void ChannelsForm::setBrokenChannels(const QString& bodyText)
{
	const QStringList channels = bodyText.split("^", Qt::SplitBehaviorFlags::SkipEmptyParts);
	quint16  brokenMask = 0;
	for (const QString& number : channels)
	{
		brokenMask |= (1 << number.toInt());
	}

	setChannelsState(brokenMask);
}

void ChannelsForm::setChannelsState(quint16 state)
{
	uint16_t mask = 0x0001;
	for (size_t index = 0; index < CHANNEL_COUNT; index++)
	{
		setChannelState(index, (state & mask) ? ChannelState::Bad : ChannelState::Good);
		mask <<= 1;

	}
}

void ChannelsForm::setChannelState(size_t number, ChannelState state)
{
	QString name = QString::asprintf("channel_%zu", number);
	QLabel* label = ui.groupBox->findChild<QLabel*>(name);
	if (label)
	{
		label->setPixmap(icons[size_t(state)].pixmap(label->size()));
	}
}
