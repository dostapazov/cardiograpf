﻿#ifndef CHANNELSFORM_HPP
#define CHANNELSFORM_HPP

#include "ui_channelsform.h"
#include <QIcon>

class ChannelsForm : public QWidget
{
	Q_OBJECT

public:
	explicit ChannelsForm(QWidget* parent = nullptr);
	enum class ChannelState : size_t {Unknown, Bad, Good};
	void initIcons();
	void resetChannelsState();

public Q_SLOTS:

	void setBrokenChannels(const QString& bodyText);
private:
	void setChannelsState(quint16 state);
	void setChannelState(size_t number, ChannelState state);
	Ui::ChannelsForm ui;
	static constexpr size_t CHANNEL_COUNT = 8;
	QIcon icons[size_t(ChannelState::Good) + 1];

};

#endif // CHANNELSFORM_HPP
