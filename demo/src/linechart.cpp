﻿#include "linechart.hpp"
#include <QLineSeries>
#include <QShowEvent>

LineChart::LineChart(QWidget* parent ):
	QChartView(parent)
{
}

void LineChart::showEvent(QShowEvent* evt)
{
	if (!chart)
	{
		initSeries();
		setSamplesParam(1000, 1);
	}
	QChartView::showEvent(evt);
}


void LineChart::clearPoints(size_t from, size_t count)
{
	size_t x = from;
	auto beg = points.begin() + x;
	auto trf = [&x](const QPointF& )->QPointF
	{
		return QPointF(x++, std::numeric_limits<double>::quiet_NaN());

	};

	if (!count)
		count = points.size();

	auto end = std::min(beg + count, points.end());
	if (beg < end)
	{
		std::transform(beg, end, beg, trf);
	}
}

void LineChart::reset()
{
	if (!series)
		return;
	setUpdatesEnabled(false);
	currentPoint = 0;
	clearPoints(currentPoint);

	series->clear();
	if (!points.empty())
	{
		xAxis->setRange(points.first().x(), points.last().x());
	}
	yMin = StartMin();
	yMax = StartMax();
	updateAxis();
	setUpdatesEnabled(true);
}

void LineChart::autoScale(bool enable)
{
	enableAutoScale = enable;
}

void LineChart::setSamplesParam(size_t value, double scale )
{
	points.resize(value);
	yScale = scale ;

	reset();
}

QString LineChart::getSeriesName() const
{
	return  sName;
}

void LineChart::setSeriesName(const QString& name)
{
	sName = name;
	if (series)
		series->setName(name);
}

void LineChart::toggleAxisLabels()
{
	bool visible = !xAxis->labelsVisible();
	xAxis->setLabelsVisible(visible);
	yAxis->setLabelsVisible(visible);
}

void LineChart::initSeries()
{
	chart = new QChart();
	chart->setTitleFont(font());
	chart->setTheme(QChart::ChartTheme::ChartThemeBlueIcy);
	chart->setAnimationOptions(QChart::AnimationOption::NoAnimation);

	series = new QLineSeries(this);

	series->setName(sName);

	yAxis =  new QValueAxis(this);
	yAxis->setTitleFont(font());
	xAxis =  new QValueAxis(this);
	xAxis->setTitleFont(font());
	xAxis->setRange(0, 1000);
	yAxis->setRange(0, 100);

	QColor lineColor(QStringLiteral("#F1B565"));
	xAxis->setTickCount(10);
	xAxis->setGridLineColor(lineColor);
	xAxis->setTickInterval(100);

	toggleAxisLabels();

	yAxis->setTickCount(5);
	yAxis->setGridLineColor(lineColor);

	chart->addAxis(xAxis, Qt::AlignmentFlag::AlignBottom);
	chart->addAxis(yAxis, Qt::AlignmentFlag::AlignLeft);
	chart->addSeries(series);
	series->attachAxis(xAxis);
	series->attachAxis(yAxis);
	chart->setMargins(QMargins(1, 1, 1, 1));


	chart->legend()->detachFromChart();
	chart->legend()->setFont(font());
	chart->legend()->setAlignment(Qt::AlignmentFlag::AlignTop);
	chart->legend()->setContentsMargins(QMarginsF(0, 0, 0, 0));

	setChart(chart);
	setRenderHint(QPainter::RenderHint::Antialiasing);
	setChannelQuality(false);
	QGraphicsLayout* l = chart->layout();
	if (l)
	{
		l->setContentsMargins(2, 2, 2, 2);
	}
}


void LineChart::refreshMinMax()
{
	double Lo = points.front().y();
	double Hi = Lo;

	for ( auto&& pt : points)
	{
		if (std::isnan(pt.y()))
			continue;
		Lo = qMin(Lo, pt.y());
		Hi = qMax(Hi, pt.y());
	}

	yMin = Lo;
	yMax = Hi;
}

size_t LineChart::nextPoint()
{
	int pt =  currentPoint + 1;

	if (pt >= points.size())
	{
		pt -= points.size();
	}

	return pt;
}


void LineChart::updateMinMax(double value)
{
	if (std::isnan(yMin))
		yMin = value;

	if (std::isnan(yMax))
		yMax = value;

	yMin = qMin(yMin, value);
	yMax = qMax(yMax, value);
}

void  LineChart::addValues(const LineChart::Values& values)
{
	setUpdatesEnabled(false);
	clearPoints(currentPoint + values.size(), ceil(points.last().x() * 0.05));
	for (const double& y  : values)
	{
		double value = y * yScale;
		updateMinMax(value);
		points[currentPoint].setY(value + offset);
		currentPoint = nextPoint();
	}
	series->replace(points.toList());
	updateAxis();
	setUpdatesEnabled(true);
}

void  LineChart::updateAxis()
{
	double Lo = qualityGood ? yMin : -.1;
	double Hi = qualityGood ? yMax : .1;

	if (std::isnan(Lo) || std::isnan(Hi))
		return;

	if (!enableAutoScale)
	{
		Hi = Lo + yRange;
	}
	constexpr double addition = 0.05;
	yAxis->setRange(Lo - addition, Hi + addition);
}

void  LineChart::setChannelQuality(bool good)
{
	if (good != qualityGood)
	{
		qualityGood = good;
		QPen pen = series->pen();
		pen.setWidthF(.5 + good);
		pen.setColor(good ? Qt::GlobalColor::blue : Qt::GlobalColor::darkRed );
		series->setPen(pen);
		reset();
	}
}

void LineChart::setYRange(double range, double offsetY)
{
	yRange = range;
	offset = offsetY;
}

double LineChart::getSignalHeight() const
{
	return fabs(yMax - yMin );
}
