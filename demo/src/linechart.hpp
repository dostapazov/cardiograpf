﻿#ifndef LINECHART_HPP

#define LINECHART_HPP

#include <QtCore/qglobal.h>
#include <QtCharts/QtCharts>
#include <QChartView>

class LineChart: public QChartView
{
	Q_OBJECT;
public:
	using Values = QVector<double> ;
	explicit LineChart(QWidget* parent = nullptr);
	void  addValues(const Values& values);
	QString getSeriesName() const;
	void setSeriesName(const QString& name);
	void setSamplesParam(size_t value, double scale);
	void setChannelQuality(bool good);
	void toggleAxisLabels();
	void setYRange(double minY, double offsetY);
	double getSignalHeight() const;

public slots:
	void reset();
	void autoScale(bool enable);

private:
	void showEvent(QShowEvent* evt) override final;

	QString sName;
	bool qualityGood = false;
	double yScale = 1.0;
	bool enableAutoScale = true;
	double offset = .0;
	double yRange  = 1;
	static 	constexpr double StartMin() {return std::numeric_limits<double>::quiet_NaN();}
	static constexpr double StartMax() {return std::numeric_limits<double>::quiet_NaN();}
	double yMin = StartMin();
	double yMax  = StartMax();

#if QT_VERSION < QT_VERSION_CHECK(6,0,0)
	using QChart = QtCharts::QChart;
#else
	using QChart = ::QChart;
#endif
	QChart* chart = nullptr;
	QLineSeries* series = nullptr;
	QValueAxis* yAxis = nullptr;
	QValueAxis* xAxis  = nullptr;
	using  Points = QVector<QPointF> ;
	Points points;
	int currentPoint = 0;
	size_t nextPoint();

	void initSeries();
	void updateAxis();
	void clearPoints(size_t from, size_t count = 0);
	void updateMinMax(double value);
	void refreshMinMax();
};

#endif // LINECHART_HPP
