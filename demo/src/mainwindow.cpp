﻿#include "mainwindow.hpp"
#include <QJsonDocument>
#include <QJsonObject>
#include <QDateTimeAxis>
#include <QValueAxis>
#include <QTime>
#include <cassert>
#include <algorithm>

#include <server/http-api.hpp>

void MainWindow::setupUI()
{
	ui.setupUi(this);
	QStatusBar* sb = statusBar();

	modelInfo = new QLabel(sb);
	sb->addPermanentWidget(modelInfo);

	sessionInfo = new QLabel(sb);
	sb->addPermanentWidget(sessionInfo);

	timeLabel = new QLabel(sb);
	sb->addPermanentWidget(timeLabel);
}

MainWindow::MainWindow(QWidget* parent)
	: QMainWindow(parent)
{
	setupUI();
	showMaximized();

	networkActions.reset(new NetworkActions);
	networkActions->setServerPort(params.serverPort());
	initSignals();
	ui.stackedWidget->setCurrentIndex(0);
	params.setFont(font());

	params.setWindowIcon(windowIcon());
	params.serverAddr("localhost");
	ui.signalView->toggleAxisLabels();

}

MainWindow::~MainWindow()
{
	networkActions->stopSession();
}

void MainWindow::initSignals()
{
	connect(ui.tbSetup, &QAbstractButton::clicked, &params, &SetupDialog::exec);
	connect(ui.tbStartStop, &QAbstractButton::toggled, this, &MainWindow::startMonitoring);
	connect(ui.tbRawMode, &QAbstractButton::toggled, ui.signalView, &SignalView::setViewMode);
	connect(ui.tbCharts, &QAbstractButton::toggled, ui.stackedWidget, &QStackedWidget::setCurrentIndex);
	connect(ui.tbLabels, &QAbstractButton::toggled, ui.signalView, &SignalView::toggleAxisLabels);

	connect(networkActions.get(), &NetworkActions::onRespond, this, &MainWindow::onRespond) ;
	connect(networkActions.get(), &NetworkActions::sessionChanged, this, &MainWindow::onSessionChanged);
	connect(networkActions.get(), &NetworkActions::signalReadActive, this, &MainWindow::onStartReadActive);

	connect(networkActions.get(), &NetworkActions::normalizationFinished, networkActions.get(), &NetworkActions::requestBrokenChannels);
	connect(networkActions.get(), &NetworkActions::brokenChannels, ui.channels, &ChannelsForm::setBrokenChannels);
	connect(networkActions.get(), &NetworkActions::brokenChannels, ui.signalView, &SignalView::brokenChannels);
	connect(networkActions.get(), &NetworkActions::brokenChannels, networkActions.get(), &NetworkActions::signalReadRequest);

	connect(networkActions.get(), &NetworkActions::deviceInfo, this, &MainWindow::deviceInfo);
	connect(networkActions.get(), &NetworkActions::signalData, this, &MainWindow::signalData);

	connect(&params, &SetupDialog::scaleChanged, this, &MainWindow::scaleChanged);
	connect(&params, &SetupDialog::autoScale, ui.signalView, &SignalView::autoScale);
	connect(ui.signalView, &SignalView::rangeUpdate, this, &MainWindow::rangeChanged);
	connect(&sessionTimer, &QTimer::timeout, this, &MainWindow::updateSessionDuration);
}

void MainWindow::deviceInfo(const QString& devText, const QString& dres, const QString& rate)
{
	QString str = QString("Model: %1, FPS %2 %3 ").arg(devText, rate, dres);
	modelInfo->setText(str);
}

QString getRespondStatusText(int status)
{
	const char* str = "";
	switch (status)
	{
		case 0:
			str = "[ no connection ]";
			break;
		case 200:
			str = " OK";
			break;
		case 400:
			str = " Bad request";
			break;
		case 404:
			str = " Session not found";
			break;
		case 500:
			str = " INternal error";
			break;
		case 502:
			str = " Device not connected";
			break;

		case 503:
			str = " Device is busy";
			break;

		case 504:
			str = " Device connection timeout";
			break;

		default:
			break;
	}
	return QString::asprintf("%d %s", status, str);
}

void MainWindow::onRespond(const QString& what, const QByteArray& data, int status)
{
	QJsonDocument doc = QJsonDocument::fromJson(data);
	ui.output->append(QString("%1 status %2").arg(what, getRespondStatusText(status)));
	ui.output->append(doc.toJson(QJsonDocument::JsonFormat::Indented));
	if (status != 200 )
	{
		onSessionChanged(getRespondStatusText(status), QString(), true);
	}
}

void MainWindow::onStartReadActive(bool active)
{
	if (active)
	{
		networkActions->normalization(params.normalizationDuration());
	}
	else
	{
		networkActions->stopSession();
	}
}

void MainWindow::scaleChanged()
{
	ui.signalView->setYRange(params.scaleRange(),  params.offsetY());
}

void MainWindow::rangeChanged(double range)
{
	ui.range->setText(QString::asprintf("%.1lf", range));
}

void MainWindow::updateSessionDuration()
{
	long long secs = sessionDuration.elapsed() / 1000;
	lldiv_t sm = div((long long)secs, (long long)60);
	lldiv_t hm = div((long long)sm.quot, (long long)60);

	timeLabel->setText
	(
		QString::asprintf("%02d:%02d:%02d", int(hm.quot), int(hm.rem), int(sm.rem))
	);
}

void MainWindow::onSessionChanged(const QString& ssId, const QString& expiry, bool error)
{
	bool sessionActive = !ssId.isEmpty() && !error;
	ui.tbStartStop->setChecked(sessionActive);
	ui.channels->resetChannelsState();
	if (sessionActive )
	{
		QDateTime dtm = QDateTime::fromString(expiry, Qt::DateFormat::ISODate);
		sessionInfo->setText(QString("SiD %1 ends %2").arg(ssId, dtm.toString("hh:mm:ss")));
		networkActions->startSignalRead(params.readInterval(), params.sampleRate(), params.resolution());
		sessionTimer.start(1000);
		sessionDuration.start();
	}
	else
	{
		sessionTimer.stop();
		sessionDuration.invalidate();
		networkActions->stopSignalRead();
		networkActions->stopSession();
		QDateTime now = QDateTime::currentDateTime();
		QString text = QString("Session %1 finished at %2").arg((error ? " unexpectedly" : "normal")).arg(now.toString());
		sessionInfo->setText(text);
	}
}

void MainWindow::normalization()
{
	ui.channels->resetChannelsState();
	networkActions->normalization(params.normalizationDuration());
}

void MainWindow::updateSampleViewParams()
{
	size_t samplesCount =  params.signalDuration()  * params.sampleRate();
	double yScale = params.resolution() / double(1000.0);
	ui.signalView->setSamplesParams(samplesCount, yScale);
	ui.signalView->autoScale(params.getAutoScale());
	scaleChanged();
	//connect(ui.signalView, &SignalView::rangeUpdate, this, &MainWindow::rangeChanged);

}

void MainWindow::startMonitoring(bool checked)
{
	if (checked)
	{

		ui.output->clear();
		updateSampleViewParams();
		networkActions->setServerPort(params.serverPort());
		networkActions->setHost(params.serverAddr());
		networkActions->getDeviceInfo();
		networkActions->startSession( params.sessionDuration());
	}
	else
	{
		networkActions->stopSignalRead();
	}
}

void MainWindow::signalData(size_t signalCount, size_t missedSize, const QByteArrayList& dataList, const QString& bch)
{
	size_t countFromChannel = dataList.front().size() / sizeof(uint16_t);
	assert(signalCount == countFromChannel);

	SignalView::ValuesList valuesList;
	SignalView::ValuesList::value_type item(signalCount);


	for (const QByteArray& channelData : dataList)
	{
		const int16_t* beg = reinterpret_cast<const int16_t*> (channelData.constData());
		const int16_t* end = beg + signalCount;
		std::transform(beg, end, item.begin(), [](const int16_t& x)->double{return double(x);});
		valuesList.append(item);
	}

	ui.signalView->appendSignals(valuesList, missedSize);
	ui.channels->setBrokenChannels(bch);
}
