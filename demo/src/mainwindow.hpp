﻿#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QtCore/qglobal.h>
#if QT_VERSION >= 0x050000
	#include <QtWidgets/QMainWindow>
#else
	#include <QtGui/QMainWindow>
#endif

#include "ui_mainwindow.h"
#include "networkactions.hpp"
#include <setupdialog.hpp>
#include <QStatusBar>


class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget* parent = nullptr);
	virtual ~MainWindow() override;

private Q_SLOTS:
	void deviceInfo(const QString& devText, const QString& dres, const QString& rate);
	void onRespond(const QString& what, const QByteArray& data, int status);
	void onSessionChanged(const QString& ssId, const QString& expiry, bool error );
	void normalization();
	void startMonitoring(bool checked);
	void signalData(size_t signalCount, size_t missedSize, const QByteArrayList& dataList, const QString& bch);

	void onStartReadActive(bool active);
	void scaleChanged();
	void rangeChanged(double range);
	void updateSessionDuration();

private:
	Ui::MainWindow ui;
	SetupDialog params;
	QScopedPointer<NetworkActions, QScopedPointerDeleteLater> networkActions;
	QLabel* sessionInfo = nullptr;
	QLabel* modelInfo = nullptr;
	QLabel* timeLabel = nullptr;
	QElapsedTimer sessionDuration;
	QTimer sessionTimer;
	void initSignals();
	void updateSampleViewParams();
	void setupUI();
};
#endif // MAINWINDOW_HPP
