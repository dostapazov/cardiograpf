﻿#include "networkactions.hpp"
#include <QNetworkRequest>
#include <server/http-api.hpp>
#include <QJsonDocument>
#include <QJsonObject>
#include <QtEndian>
#include <QDebug>

NetworkActions::NetworkActions(QObject* parent)
	: QObject{parent}
{
	readSignalTimer.setSingleShot(true);
	readSignalTimer.setTimerType(Qt::PreciseTimer);
	connect(&readSignalTimer, &QTimer::timeout, this, &NetworkActions::signalReadRequest);
}

bool NetworkActions::isStatusOk(int status)
{
	return status == 200;
}

int NetworkActions::replyStatus(QNetworkReply* reply)
{
	if (!reply)
		return 0;
	return reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
}

bool NetworkActions::isReplyStatusOk(QNetworkReply* reply)
{
	return reply && isStatusOk(replyStatus(reply));
}

QNetworkRequest NetworkActions::makeRequest(const QStringList& pathElements)
{
	if (pathElements.empty())
		return QNetworkRequest();

	QString path;
	if (!pathElements.first().startsWith("/"))
		path = "/";

	path += pathElements.join("/");
	QUrl url(path);
	url.setScheme("http");
	url.setHost(host);
	url.setPort(port);
	QNetworkRequest req( url );
	req.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded"));
	req.setRawHeader(http_api::API_VER_KEY, http_api::API_VER_1_0_0);
	return req;
}

QNetworkRequest NetworkActions::makeRequest(const QString& path)
{
	QString div = QStringLiteral("/");
	QStringList elements = path.split(div, Qt::SplitBehaviorFlags::SkipEmptyParts);
	int index = elements.indexOf("<string>");
	if (index >= 0)
		elements.replace(index, sessionId);

	return makeRequest(elements);
}

void NetworkActions::networkError(QNetworkReply::NetworkError error)
{
	QObject* src = sender();
	QNetworkReply* reply = dynamic_cast<QNetworkReply*>(src);
	if (reply)
	{
		qDebug() << "Error :" << error << reply->request().url() << " Error : " << reply->errorString();
		reply->close();
	}
	QScopedPointerDeleteLater::cleanup(src);
}

void NetworkActions::setupReplyHandlers(QNetworkReply* reply, ReplyHandler handler)
{
	connect(reply, &QNetworkReply::finished, this, handler);
	connect(reply, &QNetworkReply::errorOccurred, this, &NetworkActions::networkError);
}

void NetworkActions::getDeviceInfo()
{
	QNetworkReply* repl = manager.get(makeRequest(http_api::PATH_DEVICE));
	setupReplyHandlers(repl, &NetworkActions::deviceInfoRespond);
}

void NetworkActions::deviceInfoRespond()
{
	QObject* src = sender();
	QNetworkReply* reply = dynamic_cast<QNetworkReply*>(src);
	QScopedPointerDeleteLater::cleanup(sender());
	if (!reply)
		return;

	QByteArray content = reply->readAll();
	int status = replyStatus(reply);
	emit onRespond("Device info", content, status, QPrivateSignal());
	QJsonObject json = jsonFromContent(content)[http_api::DEV_FEATURES].toObject();
	QString modelName = QString::fromUtf8(json[http_api::DEV_MODEL ].toVariant().toByteArray()).trimmed();
	QString devVersion = QString::fromUtf8(json[http_api::DEV_VERSION ].toVariant().toByteArray()).trimmed();
	QString inv = QString::fromUtf8(json[http_api::DEV_INVENTORY].toVariant().toByteArray()).trimmed();

	QString modelInfo = QString ("%1 V %2").arg(modelName, devVersion);

	if (!inv.isEmpty())
	{
		modelInfo += QString::fromStdWString(L" № - %1").arg(inv);
	}

	QString dres = QString::asprintf("%.2lf mV", json[http_api::DEV_DIGIT_RES].toDouble())  ;
	QString rate = QString::asprintf("%d", json[http_api::DEV_SAMPLE_RATE].toInt());

	emit deviceInfo(modelInfo, dres, rate);
}

QByteArray NetworkActions::toJson(const QVariantMap& data)
{
	return QJsonDocument
		   (
			   QJsonObject::fromVariantMap(data)
		   ).toJson(QJsonDocument::JsonFormat::Compact) ;
}

QJsonObject NetworkActions::jsonFromContent(const QByteArray& content)
{
	return QJsonDocument::fromJson(content).object();
}

void NetworkActions::startSession(int duration)
{
	QVariantMap sessionReq;
	sessionReq[http_api::SESSION_LENGTH] = duration;
	QNetworkRequest req = makeRequest(http_api::PATH_SESSION);
	req.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded"));
	QNetworkReply* repl = manager.post(req, toJson(sessionReq) );
	setupReplyHandlers(repl, &NetworkActions::startSessionRespond);
}

void NetworkActions::startSessionRespond()
{
	QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());
	QScopedPointerDeleteLater::cleanup(sender());
	QByteArray content = reply->readAll();
	int status = replyStatus(reply);
	emit onRespond("Statr session", content, status, QPrivateSignal());

	if (status == QNetworkReply::NetworkError::ServiceUnavailableError)
		return clearSession(true);

	if (isStatusOk(status))
	{
		QVariantMap vm = jsonFromContent(content)[http_api::SESSION].toObject().toVariantMap();
		if (vm.contains(http_api::SESSION_ID))
		{
			sessionId = vm[http_api::SESSION_ID].toString();
			sessionExpiry =  vm[http_api::SESSION_EXPIRY].toString();
			emit sessionChanged(sessionId, sessionExpiry, false, QPrivateSignal());
		}
	}
}

void NetworkActions::stopSession()
{
	if (sessionId.isEmpty())
		return;

	QNetworkReply* repl = manager.deleteResource(makeRequest({QString(http_api::PATH_SESSION), sessionId}));
	setupReplyHandlers(repl, &NetworkActions::stopSessionRespond);
}

void NetworkActions::stopSessionRespond()
{
	QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());
	QScopedPointerDeleteLater::cleanup(sender());
	int status = replyStatus(reply);
	emit onRespond("Stop session", QByteArray(), status, QPrivateSignal());
	return clearSession(false);
}

void NetworkActions::extendSession()
{
	if (sessionId.isEmpty())
		return;

	QNetworkReply* repl = manager.put(makeRequest({QString(http_api::PATH_SESSION), sessionId}), QByteArray());
	setupReplyHandlers(repl, &NetworkActions::extendSessionRespond);
}

void NetworkActions::extendSessionRespond()
{
	QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());
	QScopedPointerDeleteLater::cleanup(sender());
	int status = replyStatus(reply);
	emit onRespond("Extend session", QByteArray(), status, QPrivateSignal());

	if (status == QNetworkReply::NetworkError::ServiceUnavailableError)
		return clearSession(true);


	if (isStatusOk(status))
	{
		emit sessionExtented(QPrivateSignal());
	}
}

void NetworkActions::normalization(int duration)
{
	if (sessionId.isEmpty())
		return;

	QVariantMap body;

	if (QDateTime::currentSecsSinceEpoch() & 1)
	{
		// for test purpose
		body[http_api::NORMALIZATION_INTERVAL] = QString::asprintf("\'%d\'", duration);
	}
	else
	{
		body[http_api::NORMALIZATION_INTERVAL] = duration;
	}
	QNetworkRequest req = makeRequest(QString(http_api::PATH_SESSION_DEVICE_NORMALIZATION));

	QNetworkReply* reply = manager.post
						   (
							   req,
							   toJson(body)
						   );
	setupReplyHandlers(reply, &NetworkActions::normalizationReply);
	readSignalTimer.stop();

}

void NetworkActions::normalizationReply()
{
	QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());
	QScopedPointerDeleteLater::cleanup(sender());

	int status = replyStatus(reply);
	emit onRespond("Norlmalization", QByteArray(), status, QPrivateSignal());

	if (status == QNetworkReply::NetworkError::ServiceUnavailableError)
		return clearSession(true);

	if (isStatusOk(status))
	{
		emit normalizationFinished();
		readSignalTimer.start();
	}
}

void NetworkActions::requestBrokenChannels()
{
	QNetworkReply* reply = manager.get(makeRequest(http_api::PATH_SESSION_DEVICE_BROKENCHANNELS));
	setupReplyHandlers(reply, &NetworkActions::brokenChannelsReply);
}

void NetworkActions::brokenChannelsReply()
{
	QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());
	QScopedPointerDeleteLater::cleanup(sender());
	int status = replyStatus(reply);
	QByteArray body = reply->readAll();
	emit onRespond("Get broken channels", body, status, QPrivateSignal());

	if (status == QNetworkReply::NetworkError::ServiceUnavailableError)
		return clearSession(true);

	if (isStatusOk(status))
	{
		QString bch = jsonFromContent(body).toVariantMap()[http_api::BROKEN_CHANNELS].toString();
		emit brokenChannels(bch, QPrivateSignal());
	}

}

void NetworkActions::setSession(const QString& ssid, const QString& expiry, bool error)
{
	if (ssid != sessionId || expiry != sessionExpiry)
	{
		sessionId = ssid;
		sessionExpiry = expiry;
		emit sessionChanged(sessionId, sessionExpiry, error, QPrivateSignal());
	}
}

void NetworkActions::clearSession(bool error)
{
	readActive = false;
	readSignalTimer.stop();
	setSession(QString(), QString(), error);
}


void NetworkActions::stopSignalRead()
{
	readSignalTimer.stop();
	readActive = false;
	if (sessionId.isEmpty())
		return;

	QNetworkReply* reply = manager.deleteResource
						   (
							   makeRequest(QString(http_api::PATH_SESSION_DEVICE_SIGNAL))
						   );
	setupReplyHandlers(reply, &NetworkActions::stopSignalReadRespond);
}

void NetworkActions::stopSignalReadRespond()
{
	readSignalTimer.stop();
	readActive = false;

	QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());
	QScopedPointerDeleteLater::cleanup(sender());

	int status = replyStatus(reply);
	emit onRespond("stop signal read", QByteArray(), status, QPrivateSignal());
	if (!isStatusOk(status))
	{
		return clearSession(true);
	}

	emit signalReadActive(false, QPrivateSignal());
}

void NetworkActions::startSignalRead(int timeout, size_t sampleRate, double resolution)
{

	if (sessionId.isEmpty())
		return;

	readSignalTimer.setInterval(timeout);
	QVariantMap body;
	body[http_api::SIGNAL_READ_RESOLUTION] = resolution;
	body[http_api::SIGNAL_READ_SAMPLE_RATE] = int(sampleRate);
	QNetworkRequest req = makeRequest(QString(http_api::PATH_SESSION_DEVICE_SIGNAL));
	req.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded"));

	QNetworkReply* reply = manager.post(
							   req,
							   toJson(body)
						   );

	setupReplyHandlers(reply, &NetworkActions::startSignalReadReply);
}

void NetworkActions::startSignalReadReply()
{
	QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());
	QScopedPointerDeleteLater::cleanup(sender());

	int status = replyStatus(reply);
	QByteArray replyData = reply->readAll();
	emit onRespond("sart signal read", replyData, status, QPrivateSignal());

	readActive = isStatusOk(status);
	if (!readActive)
	{
		readSignalTimer.stop();
		return clearSession(true);
	}
	emit signalReadActive(readActive, QPrivateSignal());
}

void NetworkActions::signalReadRespond()
{
	QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());
	QScopedPointerDeleteLater::cleanup(sender());
	QByteArray data = reply->readAll();
	int status = replyStatus(reply);

	if (!isStatusOk(status))
		return clearSession(true);

	handleRecvSignals(data);
	readSignalTimer.start();
}

void NetworkActions::signalReadRequest()
{
	readSignalTimer.stop();
	if (readActive)
	{
		QNetworkReply* reply = manager.get(
								   makeRequest(QString(http_api::PATH_SESSION_DEVICE_SIGNAL))
							   );

		setupReplyHandlers(reply, &NetworkActions::signalReadRespond);
		return readSignalTimer.start();

	}
}

void NetworkActions::splitByChannels(size_t signalCount, size_t missed, const QByteArray& input, const QString& bch)
{
	int channelCount = input.size() / (signalCount * sizeof(uint16_t));

	QByteArrayList outList;
	outList.reserve(channelCount);


	for (int channel = 0; channel < channelCount; channel++)
	{
		const uint16_t* src = reinterpret_cast<const uint16_t*>(input.constData()) + channel;

		QByteArray channelData(signalCount * sizeof(uint16_t), Qt::Uninitialized)	;
		uint16_t* dest_beg = reinterpret_cast<uint16_t*>(channelData.data());
		uint16_t* dest_end = dest_beg + signalCount;
		while (dest_beg < dest_end)
		{
			*dest_beg = qFromLittleEndian(*src);
			++dest_beg;
			src += channelCount;
		}
		outList.append(channelData);
	}

	emit signalData(signalCount, missed, outList, bch, QPrivateSignal());
}

void NetworkActions::handleRecvSignals(const QByteArray& data)
{
	QJsonObject replJson = jsonFromContent(data);
	size_t size = replJson[http_api::SIGNAL_READ_SIZE].toInt();
	if (size)
	{
		size_t missed = replJson[http_api::SIGNAL_READ_MISSED_SIZE].toInt();
		QByteArray signalsData = QByteArray::fromBase64(replJson[http_api::SIGNAL_READ_DATA].toString().toUtf8());
		QString bch = replJson[http_api::BROKEN_CHANNELS].toString();

		emit onRespond
		(
			QString::asprintf("signals %d , missed %d, words %d", int(size), int(missed), int(signalsData.size() / sizeof(uint16_t))),
			QByteArray(), 200, QPrivateSignal()
		);

		splitByChannels(size, missed, signalsData, bch);
	}

}

void NetworkActions::setServerPort(int value)
{
	port = value;
}

int NetworkActions::getServerPort()
{
	return port ;
}


QString NetworkActions::getHost() const
{
	return host;
}

void NetworkActions::setHost(const QString& newHost)
{
	host = newHost;
}

