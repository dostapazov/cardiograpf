﻿#ifndef NETWORKACTIONS_HPP
#define NETWORKACTIONS_HPP

#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QTimer>

class NetworkActions : public QObject
{
	Q_OBJECT
public:
	explicit NetworkActions(QObject* parent = nullptr);

	void handleRecvSignals(const QByteArray& data);


	int getServerPort();
	QString getHost() const;
	void setHost(const QString& newHost);

signals:
	void deviceInfo(const QString& modelInfo, const QString& dres, const QString& rate);
	void onRespond(const QString& what, const QByteArray& json, int status, QPrivateSignal);
	void sessionChanged(const QString& sessId, const QString& expiredDate, bool error, QPrivateSignal);
	void sessionExtented(QPrivateSignal);
	void brokenChannels(const QString& state, QPrivateSignal);
	void signalData(size_t signalCount, size_t missedSize, const QByteArrayList& data, const QString& bch, QPrivateSignal);
	void normalizationFinished();
	void signalReadActive( bool active, QPrivateSignal);

public Q_SLOTS:
	void setServerPort(int value);
	void getDeviceInfo();
	void startSession(int duration);
	void stopSession();
	void extendSession();
	void normalization(int duration);
	void stopSignalRead();
	void startSignalRead(int timeout, size_t sampleRate, double resolution);
	void signalReadRequest();
	void requestBrokenChannels();

private Q_SLOTS:
	void networkError(QNetworkReply::NetworkError error);
	void deviceInfoRespond();
	void startSessionRespond();

	void stopSessionRespond();
	void extendSessionRespond();
	void normalizationReply();
	void brokenChannelsReply();
	void startSignalReadReply();
	void stopSignalReadRespond();
	void signalReadRespond();

private :
	QString host = "localhost";
	quint16 port = 8080;
	QString sessionId;
	QString sessionExpiry;
	QNetworkAccessManager manager;

	QTimer readSignalTimer;
	bool   readActive = false;

private:
	void setSession(const QString& ssid, const QString& expiry, bool error);
	void clearSession(bool error);
	static bool isStatusOk(int status);
	static bool isReplyStatusOk(QNetworkReply* reply);
	static int replyStatus(QNetworkReply* reply);
	static QJsonObject jsonFromContent(const QByteArray& content);
	static QByteArray toJson(const QVariantMap& data);

	using ReplyHandler = void (NetworkActions::*)();
	void setupReplyHandlers(QNetworkReply* reply, ReplyHandler handler);
	QNetworkRequest makeRequest(const QStringList& pathElements);
	QNetworkRequest makeRequest(const QString& path);
	void splitByChannels(size_t signalCount, size_t missed, const QByteArray& input, const QString& bch);
};

#endif // NETWORKACTIONS_HPP
