﻿#include "paramterscalculator.hpp"

/**
 * @brief ParamtersCalculator::calculate
 * @param input
 * @param out
 * calculate statements
 *  I   = L
 *  II  = F
 *  III = F-L
 *  AVR = -(F+L)/2
 *  AVL = L-F/2
 *  AVF = F-L/2
 *  V1  = C1-(L+R)/3
 *  V2  = C2-(L+R)/3
 *  V3  = C3-(L+R)/3
 *  V4  = C4-(L+R)/3
 *  V5  = C5-(L+R)/3
 *  V6  = C6-(L+R)/3
 */

void ParamtersCalculator::calculate(const input_t& input, output_t& out)
{
	constexpr double NEG_HALF = -0.5;
	constexpr double NEG_ONE_THIRD = -1.0 / 3.0;

	double LF = (*input.LR) + (*input.RF);

	*out.I   = *input.LR;
	*out.II  = *input.RF;
	*out.III = (*input.RF - *input.LR);

	*out.AVR = LF * NEG_HALF ;
	*out.AVL = (*input.LR) + NEG_HALF * (*input.RF);
	*out.AVF = (*input.RF) + (*input.LR) * NEG_HALF;

	double T = LF * NEG_ONE_THIRD;

	*out.V1 = (*input.C1) + T;
	*out.V2 = (*input.C2) + T;
	*out.V3 = (*input.C3) + T;
	*out.V4 = (*input.C4) + T;
	*out.V5 = (*input.C5) + T;
	*out.V6 = (*input.C6) + T;
}
