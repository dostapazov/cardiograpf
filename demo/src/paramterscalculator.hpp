﻿#ifndef PARAMTERSCALCULATOR_HPP
#define PARAMTERSCALCULATOR_HPP

#include <QtCore/qglobal.h>

namespace ParamtersCalculator {
struct input_t
{
	const  double*  LR;
	const  double*  RF;
	const  double*  C1;
	const  double*  C2;
	const  double*  C3;
	const  double*  C4;
	const  double*  C5;
	const  double*  C6;
	input_t& operator ++();
};

struct output_t
{
	double*  I;
	double*  II;
	double*  III;
	double*  AVR;
	double*  AVL;
	double*  AVF;
	double*  V1;
	double*  V2;
	double*  V3;
	double*  V4;
	double*  V5;
	double*  V6;
	output_t& operator ++();
};

void calculate(const input_t& input, output_t& out);

inline input_t& input_t::operator ++()
{
	++LR;
	++RF;
	++C1;
	++C2;
	++C3;
	++C4;
	++C5;
	++C6;
	return *this;
}

inline output_t& output_t::operator ++()
{
	++ I;
	++  II;
	++  III;
	++  AVR;
	++  AVL;
	++  AVF;
	++  V1;
	++  V2;
	++  V3;
	++  V4;
	++  V5;
	++  V6;
	return *this;
}

}

#endif // PARAMTERSCALCULATOR_HPP
