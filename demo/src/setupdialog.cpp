﻿#include "setupdialog.hpp"
#include "qwidget.h"

SetupDialog::SetupDialog(QWidget* parent) :
	QDialog(parent)
{
	ui.setupUi(this);
	connect(ui.scaleRange, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SetupDialog::scaleChanged);
	connect(ui.offsetY, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SetupDialog::scaleChanged);
	connect(ui.cbAutoScale, &QCheckBox::toggled, this, &SetupDialog::autoScale);
}

QString SetupDialog::serverAddr() const
{
	return ui.server->text();
}

void SetupDialog::serverAddr(const QString& addr)
{
	ui.server->setText(addr);
}

int SetupDialog::serverPort() const
{
	return ui.serverPort->value();
}

void SetupDialog::setServerPort(int port)
{
	ui.serverPort->setValue(port);
}

double SetupDialog::resolution()
{
	return ui.resolution->value();
}

int SetupDialog::sampleRate()
{
	return ui.sampleRate->currentText().toInt();
}

int SetupDialog::sessionDuration()
{
	return ui.sessionLength->value();
}

int SetupDialog::signalDuration()
{
	return ui.signalDuration->value();
}

int SetupDialog::readInterval()
{
	return ui.getInterval->value();
}

int SetupDialog::normalizationDuration()
{
	return ui.normalizationDuration->value();
}

double SetupDialog::scaleRange()
{
	return ui.scaleRange->value();
}

double SetupDialog::offsetY()
{
	return ui.offsetY->value();
}

bool SetupDialog::getAutoScale() const
{
	return ui.cbAutoScale->isChecked();
}

