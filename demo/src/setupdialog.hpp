﻿#ifndef SETUPDIALOG_HPP
#define SETUPDIALOG_HPP

#include "ui_setupdialog.h"

class SetupDialog : public QDialog
{
	Q_OBJECT

public:
	explicit SetupDialog(QWidget* parent = nullptr);
	int serverPort() const;
	void setServerPort(int  port);
	QString serverAddr() const;
	void serverAddr(const QString& addr );

	double resolution();

	int sampleRate();
	int sessionDuration();
	int signalDuration();
	int readInterval();
	int normalizationDuration();
	double scaleRange();
	double offsetY();
	bool getAutoScale()  const;
Q_SIGNALS:
	void scaleChanged();
	void autoScale(bool enable);
private:
	Ui::SetupDialog ui;
};

#endif // SETUPDIALOG_HPP
