﻿#include "signalview.hpp"
#include "paramterscalculator.hpp"

SignalView::SignalView(QWidget* parent) :
	QWidget(parent)
{
	ui.setupUi(this);
	initChannels();
	setViewMode(false);
}

bool SignalView::getViewMode()
{
	return rawMode;
}

void SignalView::toggleAxisLabels()
{
	for ( auto&& lc : charts)
	{
		lc->toggleAxisLabels();
	}
}

void SignalView::setViewMode(bool mode)
{
	rawMode = mode;

	setSignalNames
	(
		rawMode ?
		QStringList{"RF", "LR", "C1", "C2", "C3", "C4", "C5", "C6"}
		:
		QStringList{"I", "II", "III", "AVR", "AVL", "AVF", "V1", "V2", "V3", "V4", "V5", "V6"}
	);
	doLayoutCharts();

}

void SignalView::brokenChannels(const QString& state)
{
	if (channelsState != state)
	{
		channelsState = state.trimmed();
		bool good = channelsState.isEmpty();
		for ( auto&& lc : charts)
		{
			lc->setChannelQuality(good);
		}
	}
}

void SignalView::autoScale(bool enable)
{
	for ( auto&& lc : charts)
	{
		lc->autoScale(enable);
	}
}

void SignalView::doLayoutCharts()
{
	setUpdatesEnabled(false);
	QLayoutItem* item;
	while ((item = ui.gridLayout->takeAt(0)))
	{
		QWidget* w = item->widget();
		w->hide();
		delete item;
	}

	int divisor = getChannelCount() >> 1;
	for (int index = 0; index < getChannelCount() ; index ++)
	{
		LineChart* lc = charts[index];
		div_t dt = div(index, divisor);
		ui.gridLayout->addWidget(lc, dt.rem, dt.quot);
		lc->reset();
		lc->show();
	}
	setUpdatesEnabled(true);
}

void SignalView::initChannels()
{
	rawMode = false;
	stdSignals.resize(getChannelCount());
	charts.resize(getChannelCount());

	std::generate(charts.begin(), charts.end(), [&]()->LineChart*
	{
		auto ch = new LineChart(this);
		ch->setFont(this->font());
		return ch;
	}
				 );
}

int SignalView::getChannelCount()
{
	constexpr size_t RAW_SIGNALS_COUNT = 8;
	constexpr size_t SIGNALS_COUNT = 12;
	return  rawMode ? RAW_SIGNALS_COUNT : SIGNALS_COUNT;
}

void SignalView::setYRange(double range,  double offsetY)
{
	for ( auto&& lc : charts)
	{
		lc->setYRange(range, offsetY);
	}

}

void SignalView::setSamplesParams(size_t samples, double scale)
{
	for ( auto&& lc : charts)
	{
		lc->setSamplesParam(samples, scale);
	}
}


LineChart* SignalView::lineChartAt(int index)
{
	return  index < getChannelCount() ?  charts[index] : nullptr;
}

void SignalView::setSignalName(int index, const QString& name)
{
	LineChart* lc = lineChartAt(index);
	if (lc)
	{
		lc->setVisible(!name.isEmpty());
		lc->setSeriesName(name);
	}
}

void SignalView::setSignalNames(const QStringList& names)
{
	int index = 0;
	for (const QString& name : names)
	{
		setSignalName(index++, name);
	}
}

void SignalView::appendValues( int index, const LineChart::Values& data)
{

	LineChart* lc = lineChartAt(index);
	if (lc)
	{
		lc->addValues(data);
	}
}

void SignalView::calculateStdSignals(const SignalView::ValuesList& rawData, size_t missedSize)
{
	size_t inputSamples = rawData.front().size();
	for (LineChart::Values& values : stdSignals)
	{
		values.resize( inputSamples + missedSize);
		std::fill(values.begin(), values.begin() + missedSize, std::numeric_limits<LineChart::Values::value_type>::quiet_NaN());
	}

	namespace  pc = ParamtersCalculator;

	pc::input_t input
	{
		rawData.at(RawSignalsOffset::RF).constData(),
		rawData.at(RawSignalsOffset::LF).constData(),
		rawData.at(RawSignalsOffset::C1).constData(),
		rawData.at(RawSignalsOffset::C2).constData(),
		rawData.at(RawSignalsOffset::C3).constData(),
		rawData.at(RawSignalsOffset::C4).constData(),
		rawData.at(RawSignalsOffset::C5).constData(),
		rawData.at(RawSignalsOffset::C6).constData()
	};


	pc::output_t output
	{
		stdSignals[SignalOffset::I].data() + missedSize,
		stdSignals[SignalOffset::II].data() + missedSize,
		stdSignals[SignalOffset::III].data() + missedSize,
		stdSignals[SignalOffset::AVR].data() + missedSize,
		stdSignals[SignalOffset::AVL].data() + missedSize,
		stdSignals[SignalOffset::AVF].data() + missedSize,
		stdSignals[SignalOffset::V1].data() + missedSize,
		stdSignals[SignalOffset::V2].data() + missedSize,
		stdSignals[SignalOffset::V3].data() + missedSize,
		stdSignals[SignalOffset::V4].data() + missedSize,
		stdSignals[SignalOffset::V5].data() + missedSize,
		stdSignals[SignalOffset::V6].data() + missedSize
	};

	for (size_t counter = 0; counter < inputSamples; counter ++)
	{
		pc::calculate(input, output);
		++input;
		++output;
	}
}

void SignalView::appendSignals(const SignalView::ValuesList& inputList, size_t missedSize)
{
	setUpdatesEnabled(false);

	if (!rawMode)
	{
		calculateStdSignals(inputList, missedSize);
	}
	const SignalView::ValuesList& sourceList = rawMode ? inputList : stdSignals;
	int number = 0;
	for (const LineChart::Values& values : sourceList)
	{
		appendValues(number++, values);
	}
	double range = lineChartAt(rawMode ? 0 : 1)->getSignalHeight();
	emit rangeUpdate(range);
	setUpdatesEnabled(true);
}

