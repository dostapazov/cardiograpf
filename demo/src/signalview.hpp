﻿#ifndef SIGNALVIEW_HPP
#define SIGNALVIEW_HPP

#include "ui_signalview.h"
#include "linechart.hpp"

class SignalView : public QWidget
{
	Q_OBJECT

public:
	using ValuesList = QVector<LineChart::Values>;
	explicit SignalView(QWidget* parent = nullptr);
	void setSignalName(int index, const QString& name);
	void setSignalNames(const QStringList& names);
	int  getChannelCount();
	void setSamplesParams(size_t samples, double scale);
	bool getViewMode();
	void setYRange(double range,  double offsetY);
Q_SIGNALS:
	void rangeUpdate(double range);
public Q_SLOTS:

	void toggleAxisLabels();
	void appendSignals(const ValuesList& inputList, size_t missedSize);
	void setViewMode(bool mode);
	void brokenChannels(const QString& state);
	void autoScale(bool enable);

private:
	enum SignalOffset : size_t  {I = 0, II, III, AVR, AVL, AVF, V1, V2, V3, V4, V5, V6};
	enum RawSignalsOffset : size_t  {RF = 0, LF, C1, C2, C3, C4, C5, C6};
	using SignalCharts = QVector<LineChart*>;

	void appendValues( int index, const LineChart::Values& data);
	LineChart* lineChartAt(int index);
	void initChannels();

	Ui::SignalView ui;
	ValuesList stdSignals;
	SignalCharts charts;
	bool rawMode = false;
	QString channelsState = "*";
	void doLayoutCharts();
//	void calculateStdSignals_old(const SignalView::ValuesList& rawData, size_t missedSize);
	void calculateStdSignals(const SignalView::ValuesList& rawData, size_t missedSize);
//	bool newCalc = true;
};

#endif // SIGNALVIEW_HPP
