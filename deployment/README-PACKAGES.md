
# RPM пакеты кардиографов КРП-01, ЭК-12

- npp-monitor-1.0.0-0.src.rpm   - source rpm package
- LEAP_15_2/npp-monitor-1.0.0-0.x86_64.rpm - binary rpm for OpenSUSE-leap-15.2
- LEAP_42_3/npp-monitor-1.0.0-0.x86_64.rpm - binary rpm for OpenSUSE-leap-42.3
