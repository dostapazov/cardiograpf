#!/bin/bash

SVC_NAME=npp-monitor
PKG_NAME=npp-monitor
PKG_VER=1.0.0
SRC_NAME=${PKG_NAME}-${PKG_VER}

SCRIPT_DIR=$(dirname "$0")
rpm_root=~/rpmbuild
rpmdev-setuptree


function check_exit()
{
  local ERR_CODE="$1"
  if [[ $ERR_CODE != "0" ]]; then
    echo "$2" error $ERR_CODE
    popd
    exit 1
  fi
}

mkdir -p ${SCRIPT_DIR}/${SRC_NAME}

pushd $SCRIPT_DIR

cp -r ../../libs ${SRC_NAME}/
cp -r ../../src ${SRC_NAME}/
cp -r ../../projects ${SRC_NAME}/


tar --create --file ${rpm_root}/SOURCES/${SRC_NAME}.tar.gz ${SRC_NAME}
#cp ./${PKG_NAME}.spec ${rpm_root}/SPECS/

rpmlint ./${PKG_NAME}.spec
check_exit "$?" rmplint

#rpmbuild -bs ${rpm_root}/SPECS/${PKG_NAME}.spec
rpmbuild -bs ./${PKG_NAME}.spec
check_exit "$?" rpmbuild
rm -r -f ${SRC_NAME}

popd
mv ${rpm_root}/SRPMS/*.src.rpm ./


