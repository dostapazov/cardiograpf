#
# spec file for package npp-monitor
#
# Copyright (c) 2023 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


Name:		npp-monitor
Version:	1.0.0
Release:	0
Group:		System/Daemons
Summary:	npp-monitor service
License:	GPL
Vendor:		Scientific production enterprise Monitor
Source0:	%{name}-%{version}.tar.gz

Prefix:		%{/opt/emias}
BuildRoot:	%{_tmppath}/%{name}-%{version}


#Requires:	libQt5Core5
#Requires:	libQt5SerialPort5
#Requires:	libQt5Concurrent5
BuildRequires:	cpp
#BuildRequires:	libQt5Core-devel
#BuildRequires:	libQt5Concurrent-devel
#BuildRequires:	libqt5-qtserialport-devel

%description
Cardiograph user space driver for model EK12T, KRP-01, KRB-01

%prep

%setup -q -n %{name}-%{version}

%build

qmake-qt5 projects/device-service/device-service.pro > /dev/null
make > /dev/null
strip npp-monitor

%install

DEST_DIR=${RPM_BUILD_ROOT}/opt/emias
mkdir -p ${DEST_DIR}
cp npp-monitor ${DEST_DIR}/
cp -r src/etc ${RPM_BUILD_ROOT}/

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/opt/emias/npp-monitor
/etc/systemd/system/npp-monitor.service
%config /etc/%{name}.conf

%pre
systemctl stop npp-monitor.service || true

%post
systemctl enable --now npp-monitor.service

%preun
systemctl disable --now npp-monitor.service

%changelog
* Fri Nov 17 2023 root
- Fix reqest extra data
- Fix start/stop logic 
* Mon Oct 30 2023 root
- Add api version headers : 'accept':'1.0.0'
* Sat Oct 21 2023 root
- Fix inventory number 
- Fix convert json values to numeric
- Fix repond BAD_REQUEST to non existent http api
* Sun Sep 3 2023 root
- First rpm package
- First version package
- First version npp-monitor
