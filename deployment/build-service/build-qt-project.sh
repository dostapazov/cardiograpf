#!/bin/bash

if [ -z "$1" ]; then

  echo "usage : " $(basename "$0") "path-to-project"
  exit 1
fi

QMAKE=qmake
if ! which $QMAKE ; then
  QMAKE=${QMAKE}-qt5
fi
echo using $QMAKE
$QMAKE --version
$QMAKE "$1" &&
make 

