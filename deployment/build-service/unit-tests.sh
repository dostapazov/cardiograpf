#!/bin/bash

script_path=$(dirname "$0")

${script_path}/build-qt-project.sh  projects/test/unit_tests.pro && \
resampler/resampler -o resampler.xml,xunitxml && \
transport/transport -o transport.xml,xunitxml && \
paramcalc/paramcalc -o paramcalc.xml,xunitxml
