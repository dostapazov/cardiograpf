#!/bin/bash
GITLAB_PROJECT=registry.gitlab.com/dostapazov/cardiograpf
PACKAGE_BUILDER_TAG_1_0="$GITLAB_PROJECT"/package-builder:1.0
QT_STATIC_BUILDER_TAG_1_0="$GITLAB_PROJECT"/qt-static-builder:1.0


QT_STATIC_BUILDER_TAG_42_3="$GITLAB_PROJECT"/qt-static-builder:42.3

#docker build -t $PACKAGE_BUILDER_TAG_1_0 --compress -f package-builder-1-0.dockerfile .
#docker push "$PACKAGE_BUILDER_TAG_1_0"

#docker build -t $QT_STATIC_BUILDER_TAG_1_0 --compress -f qt-static-builder-1-0.dockerfile . || exit 
#docker push "$QT_STATIC_BUILDER_TAG_1_0"

#docker build -t $PACKAGE_BUILDER_TAG_0_5 --compress -f package-builder-0-5.dockerfile .
#docker push "$PACKAGE_BUILDER_TAG_0_5"

docker build -t $QT_STATIC_BUILDER_TAG_42_3 --compress -f qt-static-builder-42.3.dockerfile . || exit 
docker push "$QT_STATIC_BUILDER_TAG_42_3"

