FROM opensuse/leap:15.2

RUN zypper update && \
    zypper --non-interactive dist-upgrade && \
    zypper --non-interactive install rpm-build  rpmdevtools rpmlint

RUN zypper --non-interactive install build && \
    zypper --non-interactive install -t pattern devel_basis && \
    zypper --non-interactive install gcc-c++
