FROM registry.gitlab.com/dostapazov/cardiograpf/package-builder:1.0
COPY qt-everywhere-src-5.15.10.tar.xz qtsrc/qt-everywhere-src-5.15.10.tar.xz

RUN cd qtsrc && \
    tar -xf  qt-everywhere-src-5.15.10.tar.xz 

RUN zypper update  && zypper --non-interactive install libX11*

RUN  cd qtsrc/qt-everywhere-src-5.15.10 && \ 
    ./configure -opensource -release  -strip -static -c++std c++14 -no-opengl -no-feature-accessibility \
    -nomake examples -nomake tests -skip qt3d -skip qtandroidextras -skip qtcanvas3d -skip qtcharts -skip qtconnectivity \
    -skip qtdatavis3d -skip qtdeclarative -skip qtdoc -skip qtgamepad && \
    gmake -j 8 && \ 
    gmake -j 8 install 

RUN rm -rf  qtsrc
ENV PATH="/usr/local/Qt-5.15.10/bin:$PATH"
RUN ln -s /usr/local/Qt-5.15.10/bin/qmake /usr/local/Qt-5.15.10/bin/qmake-qt5  
