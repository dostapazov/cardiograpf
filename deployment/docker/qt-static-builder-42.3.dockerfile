FROM opensuse/leap:42.3

RUN zypper update && \
    zypper --non-interactive install rpm-build  rpmdevtools rpmlint && \
    zypper --non-interactive install build && \
    zypper --non-interactive install -t pattern devel_basis && \
    zypper --non-interactive install gcc-c++ && \
    zypper --non-interactive install python3 perl ninja

#RUN zypper update  && zypper --non-interactive install libX11*

COPY qt-everywhere-src-5.9.8.tar.xz qtsrc/qt-everywhere-src-5.9.8.tar.xz

RUN cd qtsrc && \
    tar -xf  qt-everywhere-src-5.9.8.tar.xz 


RUN  cd qtsrc/qt-everywhere-opensource-src-5.9.8 && \ 
    ./configure -opensource -confirm-license -release  -strip -static -c++std c++11 -no-opengl -no-feature-accessibility -no-gui \
    -nomake examples -nomake tests -skip qt3d -skip qtandroidextras -skip qtcanvas3d -skip qtcharts -skip qtconnectivity \
    -skip qtdatavis3d -skip qtdeclarative -skip qtdoc -skip qtgamepad && \
    gmake -j 8 && \ 
    gmake -j 8 install 

RUN rm -rf  qtsrc
ENV PATH="/usr/local/Qt-5.9.8/bin:$PATH"
RUN ln -s /usr/local/Qt-5.9.8/bin/qmake /usr/local/Qt-5.9.8/bin/qmake-qt5
