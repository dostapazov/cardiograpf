# Driver components

```plantuml
@startuml
Package "Driver Application" {

interface SerialPort as serial
component [Configurator] as conf
component [Transport] as transport
component [DeviceInterface] as DevIface
component [HttpServer] as server
component [KRP-Device]  as krpdev
component [EK12T-Device] as ektdev

conf -D--> DevIface : "Create device\nInterface"
conf -D--> server : setup listen ports
conf -D--> serial : Setup speed\nport name

serial ..R..> transport
transport ..R..> DevIface : data flow
DevIface <..R...> server
DevIface -L-> transport : configure Receive Parameters

DevIface <-D- krpdev : Generalize
DevIface <-D- ektdev : Generalize
}

@enduml

```

```plantuml
@startuml
Package "Device Interface" {
    component [DeviceInterface] {
        portin input
        portout output

        component dataParser  {
           portin inData 
           portout dataL
           portout dataR
           portout dataC1
           portout dataC2
           portout dataC3
           portout dataC4
           portout dataC5
           portout dataC6
        }

        component Resampler  {
            component L
            portin inL
            portout outL
            inL -D-> L
            L -D->outL

            component R
            portin inR
            portout outR
            inR -D-> R
            R -D->outR

            component C1
            portin inC1
            portout outC1
            inC1 -D-> C1
            C1 -D->outC1
            
            component C2
            portin inC2
            portout outC2
            inC2 -D-> C2
            C2 -D->outC2

            component C3
            portin inC3
            portout outC3
            inC3 -D-> C3
            C3 -D->outC3

            component C4
            portin inC4
            portout outC4
            inC4 -D-> C4
            C4 -D->outC4

            component C5
            portin inC5
            portout outC5
            inC5 -D-> C5
            C5 -D->outC5

            component C6
            portin inC6
            portout outC6
            inC6 -D-> C6
            C6 -D->outC6

        }

        component DataReader {
             
             portin inRead
             portout readOut
        }

        input -R-> inData
        dataL -D-> inL
        dataR -D-> inR
        dataC1 -D-> inC1
        dataC2 -D-> inC2
        dataC3 -D-> inC3
        dataC4 -D-> inC4
        dataC5 -D-> inC5
        dataC6 -D-> inC6

        outL -D-> inRead
        outR -D-> inRead
        outC1 -D-> inRead
        outC2 -D-> inRead
        outC3 -D-> inRead
        outC4 -D-> inRead
        outC5 -D-> inRead
        outC6 -D-> inRead
        readOut --> output

        
    } 
    
    

}

@enduml
```
