﻿#include "configparam.hpp"
#include <QSettings>
#include <QSettings>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QDebug>
#include <crow/logging.h>

#if QT_VERSION < QT_VERSION_CHECK(5,15,0)
	constexpr auto splitFlag = QString::SplitBehavior::SkipEmptyParts;
#else
	constexpr auto splitFlag = Qt::SplitBehaviorFlags::SkipEmptyParts;
#endif

QStringList ConfigParam::serialPortNames =
#ifdef Q_OS_WIN
{"COM"};
#else
	{"ttyUSB[0-9]+"};
#endif

constexpr int DEFAULT_DURATION = 3;
constexpr int DEFAULT_SERVER_PORT = 1080;
constexpr int DEFAULT_ZERO_OFFSET = 0x7FF;
constexpr int DEFAULT_LOG_LEVEL = static_cast<int>(crow::LogLevel::Warning);
constexpr const char* DEFAULT_INNVENTORY_NUMBER = "000-000-000";

int ConfigParam::outputDuration = DEFAULT_DURATION;
int ConfigParam::serverPort = DEFAULT_SERVER_PORT;
int ConfigParam::zeroOffset = DEFAULT_ZERO_OFFSET;
int ConfigParam::logLevel  = DEFAULT_LOG_LEVEL ;
QString ConfigParam::inventoryNumber  = DEFAULT_INNVENTORY_NUMBER;
bool ConfigParam::showReadInterval = false;

const QString& ConfigParam::getInventoryNum()
{
	return inventoryNumber;
}

bool ConfigParam::getShowReadInterval()
{
	return showReadInterval;
}

QStringList ConfigParam::getSerialPortNames()
{
	return serialPortNames;
}

int   ConfigParam::getOuputDuration()
{
	return outputDuration;
}

int ConfigParam::getServerPort()
{
	return serverPort;
}

int ConfigParam::getZeroOffset()
{
	return zeroOffset;
}

int ConfigParam::getLogLevel()
{
	return logLevel;
}

constexpr const char SERIAL_PORT_KEY[] = "serial-port";
constexpr const char DURATION_KEY[] = "duration";
constexpr const char SERVER_PORT_KEY[] = "server-port";
constexpr const char ZERO_OFFSET_KEY[] = "zero-offset";
constexpr const char LOG_LEVEL_KEY[] = "log-level";
constexpr const char INVENTORY_NUMBER_KEY[] = "inventory-number";
constexpr const char READINTERVAL_INFO_KEY[] = "print-read-interval";

void ConfigParam::parseCommandLine(const QStringList& arguments)
{
	QCommandLineParser parser;

	QCommandLineOption optSerialPort = {{"s", SERIAL_PORT_KEY }, "Serial port name template [ttyUSB0,ttyUSB*] ", SERIAL_PORT_KEY};
	QCommandLineOption optOutputDuration = {{"d", DURATION_KEY }, "output queue duration", DURATION_KEY};
	QCommandLineOption optConfigFile = {{"c", "config" }, "path to configuration file", "config file"};
	QCommandLineOption optWriteConfig = {{"w", "write-config" }, "write config to file", "config file" };
	QCommandLineOption optServerPort = {{"p", SERVER_PORT_KEY }, "http server port", SERVER_PORT_KEY};
	QCommandLineOption optZeroOffset = {{"z", ZERO_OFFSET_KEY }, "zero offset", ZERO_OFFSET_KEY};
	QCommandLineOption optLogLevel   = {{"l", LOG_LEVEL_KEY }, "log-level: 0 - DEBUG, 1 - INFO , 2 - WARNING , 3 - ERROR, 4 - CRITICAL ", LOG_LEVEL_KEY};
	QCommandLineOption optInventoryNumber   = {{"i", INVENTORY_NUMBER_KEY }, "Device invenory number", INVENTORY_NUMBER_KEY};
	QCommandLineOption optShowReadInterval = {"r", "print read interval info"};

	parser.addOptions
	(
	{
		optSerialPort, optServerPort, optOutputDuration, optConfigFile,
		optWriteConfig, optZeroOffset, optLogLevel, optShowReadInterval, optInventoryNumber
	}
	);

	parser.addHelpOption();
	parser.addVersionOption();

	parser.process(arguments);

	if (parser.isSet(optConfigFile))
	{
		readConfigFile(parser.value(optConfigFile));
	}

	if (parser.isSet(optSerialPort))
	{
		QString param = parser.value(optSerialPort);

		serialPortNames = param.split(",", splitFlag);
	}

	if (parser.isSet(optOutputDuration))
	{
		int value = parser.value(optOutputDuration).toUInt();
		if (value)
		{
			outputDuration = value;
		}
	}

	if (parser.isSet(optServerPort))
	{
		int value = parser.value(optServerPort).toUInt();
		if (value)
		{
			serverPort = value;
		}
	}

	if (parser.isSet(optZeroOffset))
	{
		bool ok {false};
		int value = parser.value(optZeroOffset).toInt(&ok);
		if (ok)
		{
			zeroOffset = value;
		}
	}

	if (parser.isSet(optLogLevel))
	{
		bool ok {false};
		int value = parser.value(optLogLevel).toInt(&ok);
		if (ok)
		{
			logLevel = value;
		}
	}

	QString value = parser.value(optInventoryNumber).trimmed();

	if (!value.isEmpty())
	{
		inventoryNumber = value;
	}

	if (parser.isSet(optShowReadInterval))
	{
		showReadInterval = true;
	}


	if (parser.isSet(optWriteConfig))
	{
		writeConfigFile(parser.value(optWriteConfig));
	}
}

void ConfigParam::readConfigFile(const QString& configPath)
{
	QSettings settings(configPath, QSettings::Format::IniFormat);
	settings.sync();
	if (settings.contains(SERIAL_PORT_KEY))
	{
		QString param = settings.value(SERIAL_PORT_KEY, "ttyUSB").toString();
		serialPortNames = param.split(",", splitFlag);
	}

	bool ok{false};

	serverPort = settings.value(SERVER_PORT_KEY, DEFAULT_SERVER_PORT).toInt(&ok);
	if (!ok)
	{
		serverPort = DEFAULT_SERVER_PORT;
		qWarning() << "Server port value is invalid. Set to default " << serverPort;
	}

	outputDuration = settings.value(DURATION_KEY, DEFAULT_DURATION).toUInt(&ok);

	if (!ok)
	{
		outputDuration = DEFAULT_DURATION;
		qWarning() << "Output duration value is invalid. Set to default " << outputDuration;
	}

	zeroOffset = settings.value(ZERO_OFFSET_KEY, DEFAULT_ZERO_OFFSET).toInt(&ok);
	if (!ok)
	{
		zeroOffset = DEFAULT_ZERO_OFFSET;
		qWarning() << "Zero offset value is invalid. Set to default " << zeroOffset;
	}

	logLevel = settings.value(LOG_LEVEL_KEY, DEFAULT_LOG_LEVEL).toInt(&ok);
	if (!ok || logLevel > static_cast<int>(crow::LogLevel::Critical))
	{
		logLevel = DEFAULT_LOG_LEVEL;
		qWarning() << "Log level value is invalid. Set to default " << logLevel;
	}

	inventoryNumber = settings.value(INVENTORY_NUMBER_KEY, DEFAULT_INNVENTORY_NUMBER).toString();
	showReadInterval = settings.value(READINTERVAL_INFO_KEY, "0").toInt();
}

void ConfigParam::writeConfigFile(const QString& configPath)
{
	QSettings settings(configPath, QSettings::Format::IniFormat);
	settings.clear();

	settings.setValue(SERIAL_PORT_KEY, serialPortNames.join(","));
	settings.setValue(SERVER_PORT_KEY, serverPort);
	settings.setValue(DURATION_KEY, outputDuration);
	settings.setValue(ZERO_OFFSET_KEY, zeroOffset);
	settings.setValue(LOG_LEVEL_KEY, logLevel);
	settings.setValue(INVENTORY_NUMBER_KEY, inventoryNumber);
	settings.setValue(READINTERVAL_INFO_KEY, int(showReadInterval));
	settings.sync();

	if (settings.status() != QSettings::Status::NoError)
	{
		qCritical() << "Settings error " << settings.status();
	}
}
