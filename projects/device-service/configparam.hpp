﻿#ifndef CONFIGPARAM_HPP
#define CONFIGPARAM_HPP

#include <QtCore/QObject>
#include <QtCore/qglobal.h>

/**
 * @brief The ConfigParam class
 * this class read configuration paramters
 * -s , --serial
 * The serial port names are a list of regular expressions
 * to filter all available serial ports on the system
 *
 * -d , --duration
 * set the capacity of output queue to save device sample for duration
 *
 * -p, --server-port
 * listen port of http server
 *
 * -c, -- config
 * path to ini file wich contains configuration parameters
 *
 * -w, --write-config
 * path to file where will save configuration parameters
 *
 * -z --zero-offset
 * offset to zero baseline
 *
 */


class ConfigParam
{
public:
	ConfigParam() = delete;
	static void parseCommandLine(const QStringList& arguments);

	static QStringList getSerialPortNames();
	static int getOuputDuration();
	static int getServerPort();
	static int getZeroOffset();
	static int getLogLevel();
	static const QString& getInventoryNum();
	static bool getShowReadInterval();

private:
	static void readConfigFile(const QString& configPath);
	static void writeConfigFile(const QString& configPath);
	static QStringList serialPortNames;
	static int  outputDuration ;
	static int  serverPort ;
	static int  zeroOffset;
	static int  logLevel ;
	static bool showReadInterval;
	static QString inventoryNumber;

};

#endif // CONFIGPARAM_HPP
