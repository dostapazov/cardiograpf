QT = core concurrent serialport

CONFIG += c++11 cmdline

QMAKE_CXXFLAGS += -Wno-deprecated-copy

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

TARGET=npp-monitor

INCLUDEPATH += \
			$${PWD}/../../src \
			$${PWD}/../../libs \
			$${PWD}/../../libs/Crow-1.0-5/include

HEADERS += \
		$${PWD}/../../src/devices/idevice.hpp \
		$${PWD}/../../src/devices/modelek12t.hpp \
		$${PWD}/../../src/devices/modelkrpb_01.hpp \
		$${PWD}/../../src/resampler/resampleimpl.hpp \
		$${PWD}/../../src/resampler/resampler.hpp \
		$${PWD}/../../src/server/http-api.hpp \
		$${PWD}/../../src/server/httpserver.hpp \
		../../src/devices/cardiographbase.hpp \
		../../src/devices/devicefinder.hpp \
		../../src/transport/transport.hpp \
		configparam.hpp \
		deviceservice.hpp

SOURCES += \
		$${PWD}/../../src/devices/modelek12t.cpp \
		$${PWD}/../../src/devices/modelkrpb_01.cpp \
		$${PWD}/../../src/resampler/resampleimpl.cpp \
		$${PWD}/../../src/resampler/resampler.cpp \
		$${PWD}/../../src/server/httpserver.cpp \
		../../src/devices/cardiographbase.cpp \
		../../src/devices/devicefinder.cpp \
		../../src/transport/transport.cpp \
		configparam.cpp \
		deviceservice.cpp \
		main.cpp


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/emias/$${TARGET}
!isEmpty(target.path): INSTALLS += target
