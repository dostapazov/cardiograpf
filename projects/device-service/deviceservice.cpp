﻿#include <deviceservice.hpp>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QtMessageHandler>
#include "configparam.hpp"
#include <server/httpserver.hpp>

#include <QDebug>
#include <csignal>

namespace {

class LogHandler : public crow::ILogHandler
{
public:
	explicit LogHandler()
	{
		crow::logger::setHandler(this);
		qInstallMessageHandler(&MessageHandler);
	}

	void log(std::string message, crow::LogLevel level) override final
	{

		QMessageLogger logger(QT_MESSAGELOG_FILE, QT_MESSAGELOG_LINE, QT_MESSAGELOG_FUNC);
		switch (level)
		{
			case crow::LogLevel::Debug :
				logger.debug() << message.data();
				break;
			case crow::LogLevel::Critical :
			case crow::LogLevel::Error :
				logger.critical() << message.data();
				break;
			case crow::LogLevel::Warning :
				logger.warning() << message.data();
				break;
			default:
				logger.info() << message.data();
				break;
		}
	}

	static void MessageHandler(QtMsgType type, const QMessageLogContext&, const QString& text);
};


void LogHandler::MessageHandler(QtMsgType type, const QMessageLogContext&, const QString& text)
{
	static const char* MsgTypeText[] =
	{
		"[ DEBUG    ] : ",
		"[ WARNING  ] : ",
		"[ CRITICAL ] : ",
		"[ !FATAL!  ] : ",
		"[ INFO     ] : "
	};

#if QT_VERSION_CHECK(5,14,0) < QT_VERSION
	constexpr auto endl = Qt::endl;
#endif

#if !defined  QT_DEBUG
	if (type == QtDebugMsg)
		return;
#endif

	QTextStream stream(stdout);
	stream << QString(MsgTypeText[size_t(type)]) << text << endl;
	stream.flush();
}

}

DeviceService::DeviceService(int& argc, char** argv):
	QCoreApplication(argc, argv)
{
	setOrganizationName("Monitor Ltd.");
	setOrganizationDomain("https://www.monitor-ltd.ru/");
	setApplicationName(QString("npp-monitor driver"));
	setApplicationVersion("1.0.0");


	initSignals();
	ConfigParam::parseCommandLine(arguments());
}

static int  exitCodeFromSignal(int sigNum)
{
	return (sigNum == SIGHUP) ? 1 : 0;
}

void DeviceService::signalHandler(int sigNum)
{
	qWarning() << "Catch system signal" << sigNum;
	int exitCode = exitCodeFromSignal(sigNum);
	if (!exitCode)
	{
		qWarning() << "Shutdown exit code " << exitCode;
	}

	QCoreApplication::instance()->exit(exitCode);
	//::exit(exitCode);
}

void DeviceService::initSignals()
{
	signal(SIGINT,  signalHandler);
	signal(SIGQUIT, signalHandler);
	signal(SIGTERM, signalHandler);
	signal(SIGHUP,  signalHandler);
	HttpServer& server = HttpServer::instance();

	connect(this, &QCoreApplication::aboutToQuit, this, &DeviceService::stop);
	connect(&deviceFinder, &DeviceFinder::deviceFound, this, &DeviceService::deviceFound);
	connect(&deviceFinder, &DeviceFinder::deviceFound, &server, &HttpServer::setDevice);
}

uint16_t DeviceService::getServerPort()
{
	return ConfigParam::getServerPort();
}

void DeviceService::start()
{
	crow::logger::setHandler(new LogHandler);
	deviceFinder.setSerialCandidateList(ConfigParam::getSerialPortNames());
	deviceFinder.start();
	processEvents();
	HttpServer& server = HttpServer::instance();
	server.setLogReadInterval(ConfigParam::getShowReadInterval());
	server.setServerPort(getServerPort());
	server.start();
	processEvents();
	server.setLogLevel(static_cast<crow::LogLevel>(ConfigParam::getLogLevel()));
}

void DeviceService::stop()
{
	qInfo() << "Stop service";
	deviceFinder.stop();
	HttpServer& server = HttpServer::instance();
	auto dev = server.getDevice();
	server.setDevice(nullptr);
	QScopedPointerDeleteLater::cleanup(dev);
	server.stop();
}

void DeviceService::deviceFound(IDevice* dev)
{
	if (dev)
	{
		dev->setOutputDuration(ConfigParam::getOuputDuration());
		dev->setZeroOffset(ConfigParam::getZeroOffset());
		dev->setInventoryNumber(ConfigParam::getInventoryNum());
		qInfo().noquote() << "Found " << dev->features().model << " at " << deviceFinder.getCurrentPortNname();
	}
	else
	{
		qWarning().noquote() << "Lost device connection " << deviceFinder.getCurrentPortNname();
	}
}

int DeviceService::execute()
{
	start();
	return exec();
}
