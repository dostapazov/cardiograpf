﻿#ifndef DEVICESERVICE_H
#define DEVICESERVICE_H

#include <QtCore/qglobal.h>
#include <QCoreApplication>
#include <QIODevice>

#include <transport/transport.hpp>
#include <devices/devicefinder.hpp>

class DeviceService : public QCoreApplication
{
	Q_OBJECT
public:
	DeviceService(int& argc, char** argv);
	int execute();
public Q_SLOTS:
	void start();
	void stop();
private Q_SLOTS:
	void deviceFound(IDevice* dev);
private:
	void initSignals();
	uint16_t getServerPort();
	static void signalHandler(int sigNum);
	DeviceFinder deviceFinder;
};

#endif // DEVICESERVICE_H
