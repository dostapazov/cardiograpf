﻿#include "deviceservice.hpp"

int main(int argc, char* argv[])
{
	DeviceService a(argc, argv);
	return a.execute();
}
