QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app
INCLUDEPATH += \
		  $${PWD}/../../../demo/src

SOURCES +=  tst_paramcalc.cpp \
	$${PWD}/../../../demo/src/paramterscalculator.cpp

HEADERS += \
	$${PWD}/../../../demo/src/paramterscalculator.hpp
