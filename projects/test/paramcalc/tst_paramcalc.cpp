﻿#include <QtTest>
#include "paramterscalculator.hpp"

// add necessary includes here

namespace pc = ParamtersCalculator;

class ParamCalc : public QObject
{
	Q_OBJECT

public:
	ParamCalc();
	~ParamCalc();

private slots:
	void calc_params_00();

	void calc_params_01();
	void calc_params_02();
	void calc_params_10();
	void calc_params_minus_10();
};

ParamCalc::ParamCalc()
{

}

ParamCalc::~ParamCalc()
{

}

void ParamCalc::calc_params_00()
{
	double in = .0;
	double res[12];
	pc::input_t input {&in, &in, &in, &in, &in, &in, &in, &in};
	pc::output_t output {res + 0, res + 1, res + 2, res + 3, res + 4, res + 5, res + 6, res + 7, res + 8, res + 9, res + 10, res + 11};
	pc::calculate(input, output);

	QVERIFY(qFuzzyCompare(res[0], .0));
	QVERIFY(qFuzzyCompare(res[1], .0));
	QVERIFY(qFuzzyCompare(res[2], .0));
	QVERIFY(qFuzzyCompare(res[3], .0));
	QVERIFY(qFuzzyCompare(res[4], .0));
	QVERIFY(qFuzzyCompare(res[5], .0));
	QVERIFY(qFuzzyCompare(res[6], .0));
	QVERIFY(qFuzzyCompare(res[7], .0));
	QVERIFY(qFuzzyCompare(res[8], .0));
	QVERIFY(qFuzzyCompare(res[9], .0));
	QVERIFY(qFuzzyCompare(res[10], .0));
	QVERIFY(qFuzzyCompare(res[11], .0));
}

void ParamCalc::calc_params_01()
{
	constexpr double in = 1;
	constexpr double CXX = in - (in + in) / 3.0;
	constexpr double AVX = in - in / 2.0;
	double res[12];
	pc::input_t input {&in, &in, &in, &in, &in, &in, &in, &in};
	pc::output_t output {res + 0, res + 1, res + 2, res + 3, res + 4, res + 5, res + 6, res + 7, res + 8, res + 9, res + 10, res + 11};
	pc::calculate(input, output);

	QVERIFY(qFuzzyCompare(res[0], in));
	QVERIFY(qFuzzyCompare(res[1], in));
	QVERIFY(qFuzzyCompare(res[2], in - in));
	QVERIFY(qFuzzyCompare(res[3], -in));
	QVERIFY(qFuzzyCompare(res[4], AVX));
	QVERIFY(qFuzzyCompare(res[5], AVX));
	QVERIFY(qFuzzyCompare(res[6], CXX));
	QVERIFY(qFuzzyCompare(res[7], CXX));
	QVERIFY(qFuzzyCompare(res[8], CXX));
	QVERIFY(qFuzzyCompare(res[9], CXX));
	QVERIFY(qFuzzyCompare(res[10], CXX));
	QVERIFY(qFuzzyCompare(res[11], CXX));
}


void ParamCalc::calc_params_02()
{
	constexpr double in = 2;
	constexpr double CXX = in - (in + in) / 3.0;
	constexpr double AVX = in - in / 2.0;
	double res[12];
	pc::input_t input {&in, &in, &in, &in, &in, &in, &in, &in};
	pc::output_t output {res + 0, res + 1, res + 2, res + 3, res + 4, res + 5, res + 6, res + 7, res + 8, res + 9, res + 10, res + 11};
	pc::calculate(input, output);


	QVERIFY(qFuzzyCompare(res[0], in));
	QVERIFY(qFuzzyCompare(res[1], in));
	QVERIFY(qFuzzyCompare(res[2], in - in));
	QVERIFY(qFuzzyCompare(res[3], -in));
	QVERIFY(qFuzzyCompare(res[4], AVX));
	QVERIFY(qFuzzyCompare(res[5], AVX));
	QVERIFY(qFuzzyCompare(res[6], CXX));
	QVERIFY(qFuzzyCompare(res[7], CXX));
	QVERIFY(qFuzzyCompare(res[8], CXX));
	QVERIFY(qFuzzyCompare(res[9], CXX));
	QVERIFY(qFuzzyCompare(res[10], CXX));
	QVERIFY(qFuzzyCompare(res[11], CXX));
}

void ParamCalc::calc_params_10()
{
	constexpr double in = 10;
	constexpr double CXX = in - (in + in) / 3.0;
	constexpr double AVX = in - in / 2.0;
	double res[12];
	pc::input_t input {&in, &in, &in, &in, &in, &in, &in, &in};
	pc::output_t output {res + 0, res + 1, res + 2, res + 3, res + 4, res + 5, res + 6, res + 7, res + 8, res + 9, res + 10, res + 11};
	pc::calculate(input, output);


	QVERIFY(qFuzzyCompare(res[0], in));
	QVERIFY(qFuzzyCompare(res[1], in));
	QVERIFY(qFuzzyCompare(res[2], in - in));
	QVERIFY(qFuzzyCompare(res[3], -in));
	QVERIFY(qFuzzyCompare(res[4], AVX));
	QVERIFY(qFuzzyCompare(res[5], AVX));
	QVERIFY(qFuzzyCompare(res[6], CXX));
	QVERIFY(qFuzzyCompare(res[7], CXX));
	QVERIFY(qFuzzyCompare(res[8], CXX));
	QVERIFY(qFuzzyCompare(res[9], CXX));
	QVERIFY(qFuzzyCompare(res[10], CXX));
	QVERIFY(qFuzzyCompare(res[11], CXX));
}

void ParamCalc::calc_params_minus_10()
{
	constexpr double in = -10;
	constexpr double CXX = in - (in + in) / 3.0;
	constexpr double AVX = in - in / 2.0;
	double res[12];
	pc::input_t input {&in, &in, &in, &in, &in, &in, &in, &in};
	pc::output_t output {res + 0, res + 1, res + 2, res + 3, res + 4, res + 5, res + 6, res + 7, res + 8, res + 9, res + 10, res + 11};
	pc::calculate(input, output);


	QVERIFY(qFuzzyCompare(res[0], in));
	QVERIFY(qFuzzyCompare(res[1], in));
	QVERIFY(qFuzzyCompare(res[2], in - in));
	QVERIFY(qFuzzyCompare(res[3], -in));
	QVERIFY(qFuzzyCompare(res[4], AVX));
	QVERIFY(qFuzzyCompare(res[5], AVX));
	QVERIFY(qFuzzyCompare(res[6], CXX));
	QVERIFY(qFuzzyCompare(res[7], CXX));
	QVERIFY(qFuzzyCompare(res[8], CXX));
	QVERIFY(qFuzzyCompare(res[9], CXX));
	QVERIFY(qFuzzyCompare(res[10], CXX));
	QVERIFY(qFuzzyCompare(res[11], CXX));
}

QTEST_APPLESS_MAIN(ParamCalc)

#include "tst_paramcalc.moc"
