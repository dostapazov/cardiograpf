QT += testlib
QT -= gui
CONFIG += c++11

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app
INCLUDEPATH += \
			$${PWD}/../../../src \
			$${PWD}/../../../libs

SOURCES +=  tst_testresampler.cpp \
	../../../src/resampler/resampleimpl.cpp \
	../../../src/resampler/resampler.cpp

HEADERS += \
	../../../src/resampler/resampleimpl.hpp \
	../../../src/resampler/resampler.hpp
