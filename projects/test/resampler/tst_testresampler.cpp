﻿#include <QtTest>
#include <resampler/resampler.hpp>
#include <resampler/resampleimpl.hpp>
#include <QDebug>
#include <algorithm>

extern "C" {

	int farrow_lagrange(double* s, int n, double p, double q,
						double frd, double** y, int* ny);
	int farrow_spline(double* s, int n, double p, double q,
					  double frd, double** y, int* ny);
}

// add necessary includes here

class ResamplerTest : public Resampler
{
public:
	ResamplerTest() = default;
	using  Resampler::InternalData ;
	using  Resampler::Values ;

};

class TestResampler : public QObject
{
	Q_OBJECT

public:
	TestResampler();
	~TestResampler();

private slots:
	void test_Recalculate();
	void test_LagrangeResampler();
	void test_SplineResampler();

	void test_InputStartedFromHiValue();
	void test_LagrangeResamplerFloat();
	void test_InputStartedFromLoValue();
private:
	static constexpr size_t INPUT_DISCR = 400;
	static constexpr size_t INPUT_RES = 5.0;
	static constexpr size_t OUTPUT_DISCR = 500;
	static constexpr size_t OUTPUT_RES = 10.0;
	ResamplerTest  testing;
	static ResamplerTest::Values generateInput(double freq, size_t discr = INPUT_DISCR, size_t size = INPUT_DISCR);
};

TestResampler::TestResampler()
{
	testing.setInputParameters(INPUT_DISCR, INPUT_RES);
	testing.setOutputParameters(OUTPUT_DISCR, OUTPUT_RES);
}

TestResampler::~TestResampler()
{
}

void TestResampler::test_Recalculate()
{
	double res = ResamplerTest::recalc(1, 1.0);
	QVERIFY(ResamplerTest::isEqual(res, 1.0));

	res = ResamplerTest::recalc(1, 10.0 / 5.0);
	QVERIFY(ResamplerTest::isEqual(res, 2.0));

	res = ResamplerTest::recalc(10, 5.0 / 10.0);
	QVERIFY(ResamplerTest::isEqual(res, 5.0));

	res = ResamplerTest::recalc(10, 4.7 / 5);
	QVERIFY(ResamplerTest::isEqual(res, 9));

	res = ResamplerTest::recalc(10, 5 / 4.7);
	QVERIFY(ResamplerTest::isEqual(res, 11));
}


template <typename Real>
static resampleimpl::VectorOfReal<Real> generate(Real ampl, Real offs, Real freq, size_t discrete, size_t outSize = 0 )
{
	constexpr Real PI_X2 = Real(2) * M_PI;

	Real angle = 0;
	Real step  = (freq * PI_X2) / (discrete );

	resampleimpl::VectorOfReal<Real> out;
	out.resize(outSize);

	std::generate(out.begin(), out.end(), [ &angle, step, ampl, offs]()->Real
	{
		Real res = offs + ampl * sin(angle);
		angle += step;
		if (angle > PI_X2)
			angle -= PI_X2;
		return res;
	}
				 );
	return out;
}

ResamplerTest::Values TestResampler::generateInput(double freq, size_t discr, size_t size )
{
	ResamplerTest::InternalData genData = generate(double(100), double(0), double(freq), discr, size);
	ResamplerTest::Values output(genData.size());
	std::copy(genData.cbegin(), genData.cend(), output.begin());
	return output;
}

template <typename T>
double RMS( const T* beg,  const T* end )
{
	double value = .0;
	size_t sz = std::distance(beg, end);
	while (beg < end)
	{
		double x = double(beg[0]);
		value += x * x;
		++beg;
	}
	return sqrt(value / double(sz));
}

void TestResampler::test_LagrangeResampler( )
{
	constexpr double FREQ = 30;
	constexpr size_t INPUT_DISCR = 400;
	constexpr size_t OUTPUT_DISCR = 500;

	resampleimpl::VectorOfDouble genData = generate<double>(10, 0, FREQ, INPUT_DISCR, INPUT_DISCR);

	resampleimpl::VectorOfDouble resampledData;
	resampleimpl::lagrange(genData, resampledData, OUTPUT_DISCR, INPUT_DISCR );

	double v0 = RMS(genData.data(), genData.data() + genData.size());
	double v1 = RMS(resampledData.data(), resampledData.data() + resampledData.size());
	qDebug() << "delta " << (v1 - v0);

	QVERIFY(Resampler::isEqual(v0, v1, 0.1));
}

void TestResampler::test_LagrangeResamplerFloat( )
{
	constexpr double FREQ = 30;
	constexpr size_t INPUT_DISCR = 400;
	constexpr size_t OUTPUT_DISCR = 500;

	resampleimpl::VectorOfFloat genData = generate<float>(10, 0, FREQ, INPUT_DISCR, INPUT_DISCR);

	resampleimpl::VectorOfFloat resampledData;
	resampleimpl::lagrange(genData, resampledData, OUTPUT_DISCR, INPUT_DISCR );

	float v0 = RMS(genData.data(), genData.data() + genData.size());
	float v1 = RMS(resampledData.data(), resampledData.data() + resampledData.size());
	qDebug() << "delta " << (v1 - v0);

	QVERIFY(Resampler::isEqual(v0, v1, 0.1));
}


void TestResampler::test_SplineResampler( )
{
	constexpr double FREQ = 50;
	constexpr size_t INPUT_DISCR = 400;
	constexpr size_t OUTPUT_DISCR = 500;

	resampleimpl::VectorOfDouble dataIn = generate<double>(1000, 1000, FREQ, INPUT_DISCR, INPUT_DISCR );

	resampleimpl::VectorOfDouble dataOut;
	resampleimpl::spline(dataIn, dataOut, OUTPUT_DISCR, INPUT_DISCR );

	double v0 = RMS<double>(dataIn.data(), dataIn.data() + dataIn.size());
	double v1 = RMS<double>(dataOut.data(), dataOut.data() + dataOut.size());
	double delta = v1 - v0;
	qDebug() << "RMS delta " << delta;

	QVERIFY(Resampler::isEqual(v0, v1, 0.1));
}

void TestResampler::test_InputStartedFromHiValue()
{
	resampleimpl::VectorOfDouble dataIn = {5000, 5001, 4998, 5000, 5001};
	resampleimpl::VectorOfDouble dataOut;

	resampleimpl::spline(dataIn, dataOut, 10, 5 );


	double v0 = RMS<double>(dataIn.data(), dataIn.data() + dataIn.size());
	double v1 = RMS<double>(dataOut.data(), dataOut.data() + dataOut.size());

	double delta = v1 - v0;
	qDebug() << "RMS delta " << delta;
	QVERIFY(testing.isEqual(v0, v1, 0.1));

	resampleimpl::lagrange(dataIn, dataOut, 10, 5 );
	v0 = RMS<double>(dataIn.data(), dataIn.data() + dataIn.size());
	v1 = RMS<double>(dataOut.data(), dataOut.data() + dataOut.size());
	delta = v1 - v0;
	qDebug() << "RMS delta " << delta;

	QVERIFY(testing.isEqual(v0, v1, 0.1));
}

void TestResampler::test_InputStartedFromLoValue()
{
	resampleimpl::VectorOfDouble dataIn = {-5000, -5001, -4998, -5000, -5001};
	resampleimpl::VectorOfDouble dataOut;

	resampleimpl::spline(dataIn, dataOut, 10, 5 );


	double v0 = RMS<double>(dataIn.data(), dataIn.data() + dataIn.size());
	double v1 = RMS<double>(dataOut.data(), dataOut.data() + dataOut.size());

	double delta = v1 - v0;
	qDebug() << "RMS delta " << delta;
	QVERIFY(testing.isEqual(v0, v1, 0.1));

	resampleimpl::lagrange(dataIn, dataOut, 10, 5 );
	v0 = RMS<double>(dataIn.data(), dataIn.data() + dataIn.size());
	v1 = RMS<double>(dataOut.data(), dataOut.data() + dataOut.size());
	delta = v1 - v0;
	qDebug() << "RMS delta " << delta;

	QVERIFY(testing.isEqual(v0, v1, 0.1));
}


QTEST_APPLESS_MAIN(TestResampler)

#include "tst_testresampler.moc"
