#!/bin/bash

host=localhost:1080
API_VERSION="'accept':'1.0.0'"
RetCode=""

function execRequest()
{
  local method=$1 
  local url=http://${host}/"$2"
  local data=""
  if [ ! -z "$3" ] ;then
    data="-d $3"
  fi

  curl $data -X $method -H $API_VERSION $url 2> /dev/null 
  return $?
}

function getJsonValue()
{
   echo $2 | grep -oP '"${1}":"\K[^"]+'
}

function getSessionId()
{
  local input=$1
  echo $input | grep -oP '"sessionId":"\K[^"]+'
}

function startSession()
{
  local body="{\"sessionLength\":$1}"
  RetCode=$(execRequest POST session $body)
  getSessionId $RetCode
}

function stopSession()
{
  execRequest DELETE session/$1 
}

function checkRequest()
{
  local method=$1
  local url=$2
  local data=$3
  local expectCode=$4
  answ=$(execRequest $method $url $data)
  resCode=$?
  code=$(echo $answ | awk '/Bad Request/ {print $1}')
  if [ "$code" = "$expectCode" ];then
    echo "OK" 
    return 0
  fi
  echo FAIL
  return 1
 }

SSID=$(startSession 2)

if [ -z $SSID ]; then
   echo Error open session $RetCode
   exit 1
fi

echo Session ID is $SSID
testurl=session/$SSID/failname
 
echo Test  GET $testurl  - $(checkRequest GET "$testurl" "-" 400)
echo Test  DELETE $testurl  - $(checkRequest DELETE "$testurl" "-" 400)
echo Test  PUT $testurl  - $(checkRequest PUT "$testurl" "-" 400)

testurl=session/$SSID/signal

testJson="{\"outDigitResolution\":5.1,\"outSampleRate\":500}"
echo Test start signal resolution is float  - $(checkRequest POST "$testurl" "$testJson" 400)

testJson="{\"outDigitResolution\":\"5\",\"outSampleRate\":500.7}"
echo Test start signal sample rate is float  - $(checkRequest POST "$testurl" "$testJson" 400)

testJson="{\"outDigitResolution\":5}"
echo Test start signal no sample rate - $(checkRequest POST "$testurl" "$testJson" 400)

testJson="{\"outSampleRate\":500}"
echo Test start signal no resolution - $(checkRequest POST "$testurl" "$testJson" 400)

testJson="{\"outDigitResolution\":\"string\",\"outSampleRate\":500}"
echo Test start signal resolution is string  - $(checkRequest POST "$testurl" "$testJson" 400)

testJson="{\"outDigitResolution\":0,\"outSampleRate\":500}"
echo Test start resolution 0  - $(checkRequest POST "$testurl" "$testJson" 400)

testJson="{\"outDigitResolution\":-1,\"outSampleRate\":500}"
echo Test start resolution less 0  - $(checkRequest POST "$testurl" "$testJson" 400)

testJson="{\"outDigitResolution\":6,\"outSampleRate\":500}"
echo Test start resolution 6  - $(checkRequest POST "$testurl" "$testJson" 400)


testJson="{\"outDigitResolution\":2,\"outSampleRate\":450}"
echo Test start sample rate less 500  - $(checkRequest POST "$testurl" "$testJson" 400)

testJson="{\"outDigitResolution\":3,\"outSampleRate\":600}"
echo Test start sample rate bigger 500 and less 1000  - $(checkRequest POST "$testurl" "$testJson" 400)

testJson="{\"outDigitResolution\":3,\"outSampleRate\":1500}"
echo Test start sample rate begger 1000  - $(checkRequest POST "$testurl" "$testJson" 400)

testJson="{\"outDigitResolution\":5,\"outSampleRate\":500}"
echo Test start signal res 5 rate 500  - $(checkRequest POST "$testurl" "$testJson" "")
stopSession $SSID
sleep 0.1

SSID=$(startSession 2)
testurl=session/$SSID/signal
testJson="{\"outDigitResolution\":4,\"outSampleRate\":1000}"
echo Test start signal res 4 rate 1000  - $(checkRequest POST "$testurl" "$testJson" "")
stopSession $SSID
sleep 0.1

SSID=$(startSession 2)
testurl=session/$SSID/signal
testJson="{\"outDigitResolution\":3,\"outSampleRate\":500}"
echo Test start signal res 3 rate 500  - $(checkRequest POST "$testurl" "$testJson" "")
stopSession $SSID
sleep 0.1

SSID=$(startSession 2)
testurl=session/$SSID/signal
testJson="{\"outDigitResolution\":2,\"outSampleRate\":1000}"
echo Test start signal res 2 rate 1000  - $(checkRequest POST "$testurl" "$testJson" "")
stopSession $SSID
sleep 0.1

#echo $(execRequest GET device)