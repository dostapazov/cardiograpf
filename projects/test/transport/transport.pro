QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG += c++11
CONFIG -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../../../src

SOURCES +=  tst_testtransport.cpp \
	../../../src/transport/transport.cpp

HEADERS += \
	../../../src/transport/transport.hpp
