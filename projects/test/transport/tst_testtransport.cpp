﻿#include <QtTest>
#include <QBuffer>
#include <QEventLoop>
#include <QDebug>
#include <QSignalSpy>
#include <random>
#include <transport/transport.hpp>

class TransportUnderTest : public Transport
{
public:
	TransportUnderTest() = default;
	using Transport::processReceive;
};

class TestTransport : public QObject
{
	Q_OBJECT

public:
	TestTransport();
	~TestTransport();

private slots:
	void initTestCase();
	void cleanupTestCase();
	void test_AfterInit();
	void start_shouldSetWaitStart();
	void start_shouldClearStatistic();
	void stop_shouldSetInactive();
	void wait_start_shouldIgnoreAllNonStartValues();
	void startByte_shouldSwitchToReceiveMode();
	void receive_shouldIncreasePayloadCounter();
	void receive_fullPayloadShouldWaitStop();
	void receive_frameShouldEmitSignal();
	void receive_frameShouldContainPayload();
	void receive_twoFrameShouldContainBothPayload();
	void receive_brokenFrameShouldResinchronize();
	void receive_brokenHardFrameShouldRecynchronize();
	void receive_directShouldWorkedAsSameAsReadyRead();
private:
	const uint8_t START_BYTE = '{';
	const uint8_t STOP_BYTE = '}';
	static constexpr size_t PAYLOAD_SIZE = 16;

	TransportUnderTest tested;
	Transport::Config config{START_BYTE, STOP_BYTE, PAYLOAD_SIZE};
	QBuffer dataSource;

	void writeToDataSource(const QByteArray& data);
	template <typename T>
	void writeToDataSource(const T* dataPtr, size_t size);
	QByteArray generateData(size_t size);
	std::mt19937 rnd;

};

void TestTransport::writeToDataSource(const QByteArray& data)
{
	dataSource.buffer().clear();
	dataSource.seek(0);
	dataSource.write(data);
	dataSource.seek(0);
	emit dataSource.readyRead();

}
template <typename T>
void TestTransport::writeToDataSource(const T* dataPtr, size_t size)
{
	QByteArray data(reinterpret_cast<const char*>(dataPtr), size);
	writeToDataSource(data);
}

QByteArray TestTransport::generateData(size_t size)
{
	QByteArray data(size, Qt::Uninitialized);
	std::fill(data.begin(), data.end(), rnd());
	return data;
}

TestTransport::TestTransport()
{
	rnd.seed(QDateTime::currentDateTime().toMSecsSinceEpoch());
}

TestTransport::~TestTransport()
{
}

void TestTransport::initTestCase()
{
	QVERIFY(!tested.isConfigured());
	tested.setConfig(config);
	dataSource.open(QBuffer::ReadWrite);
	tested.setIoDevice(&dataSource);
}

void TestTransport::cleanupTestCase()
{
	tested.setIoDevice(nullptr);
}

void TestTransport::test_AfterInit()
{
	QVERIFY(tested.isConfigured());
	QVERIFY(tested.getReceiveState() == Transport::ReceiveState::Inactive);
}

void TestTransport::start_shouldSetWaitStart()
{
	tested.start();
	QVERIFY(tested.getReceiveState() == Transport::ReceiveState::WaitStart);
}

void TestTransport::start_shouldClearStatistic()
{
	tested.start();
	QCOMPARE(tested.recvedBytes(), uint64_t(0));
	QCOMPARE(tested.recvedFrames(), uint64_t(0));
	QCOMPARE(tested.lostFrames(), uint64_t(0));
}

void TestTransport::stop_shouldSetInactive()
{
	tested.start();
	tested.stop();
	QVERIFY(tested.getReceiveState() == Transport::ReceiveState::Inactive);
}

void TestTransport::wait_start_shouldIgnoreAllNonStartValues()
{
	tested.start();
	writeToDataSource("abcde12345");
	QVERIFY(tested.getReceiveState() == Transport::ReceiveState::WaitStart);
	QVERIFY(tested.currentPayloadSize() == 0);
	tested.stop();
}

void TestTransport::startByte_shouldSwitchToReceiveMode()
{
	tested.start();
	writeToDataSource("{");
	QVERIFY(tested.getReceiveState() == Transport::ReceiveState::ReceivePayload);
	QVERIFY(tested.currentPayloadSize() == 0);
	tested.stop();
}

void TestTransport::receive_shouldIncreasePayloadCounter()
{
	constexpr const uint8_t payload[] = "123";

	tested.start();
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(payload, sizeof(payload));
	QVERIFY(tested.getReceiveState() == Transport::ReceiveState::ReceivePayload);
	QVERIFY(tested.currentPayloadSize() == sizeof(payload));
	tested.stop();
}

void TestTransport::receive_fullPayloadShouldWaitStop()
{
	constexpr const uint8_t payload[PAYLOAD_SIZE] = "123";
	tested.start();
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(payload, sizeof(payload));
	QVERIFY(tested.getReceiveState() == Transport::ReceiveState::WaitStop);
	tested.stop();
}

void TestTransport::receive_frameShouldEmitSignal()
{
	constexpr  uint64_t EXPECTED_RECVED_BYTES = sizeof(START_BYTE) + PAYLOAD_SIZE + sizeof(STOP_BYTE);
	QByteArray payload = generateData(PAYLOAD_SIZE);
	QSignalSpy spy(&tested, &Transport::frameRecved);

	tested.start();

	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(payload);
	writeToDataSource(&STOP_BYTE, sizeof(STOP_BYTE));
	QVERIFY(spy.count() == 1);

	QCOMPARE(tested.recvedFrames(), uint64_t(1));
	QVERIFY(tested.recvedBytes() == EXPECTED_RECVED_BYTES);

	tested.stop();
}

void TestTransport::receive_directShouldWorkedAsSameAsReadyRead()
{
	constexpr  uint64_t EXPECTED_RECVED_BYTES = sizeof(START_BYTE) + PAYLOAD_SIZE + sizeof(STOP_BYTE);
	QByteArray payload = generateData(PAYLOAD_SIZE);
	QSignalSpy spy(&tested, &Transport::frameRecved);

	tested.start();

	tested.processReceive(QByteArray(reinterpret_cast<const char*>(&START_BYTE), sizeof(START_BYTE)));
	tested.processReceive(payload);
	tested.processReceive(QByteArray(reinterpret_cast<const char*>(&STOP_BYTE), sizeof(STOP_BYTE)));

	QVERIFY(spy.count() == 1);

	QCOMPARE(tested.recvedFrames(), uint64_t(1));
	QCOMPARE(tested.recvedBytes(), EXPECTED_RECVED_BYTES);

	tested.stop();
}

void TestTransport::receive_frameShouldContainPayload()
{
	QByteArray payload = generateData(PAYLOAD_SIZE);
	QSignalSpy spy(&tested, &Transport::frameRecved);

	tested.start();
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(payload);
	writeToDataSource(&STOP_BYTE, sizeof(STOP_BYTE));
	auto list = spy.at(0);
	QCOMPARE(list.at(0).toByteArray(), payload);
	tested.stop();
}

void TestTransport::receive_twoFrameShouldContainBothPayload()
{
	QByteArray payload1 = generateData(PAYLOAD_SIZE);
	QByteArray payload2 = generateData(PAYLOAD_SIZE);
	QSignalSpy spy(&tested, &Transport::frameRecved);

	tested.start();
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(payload1);
	writeToDataSource(&STOP_BYTE, sizeof(STOP_BYTE));
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(payload2);
	writeToDataSource(&STOP_BYTE, sizeof(STOP_BYTE));

	// Expect Two signals
	QVERIFY(spy.count() == 2);

	// Compare payloads
	QCOMPARE(spy.at(0).at(0).toByteArray(), payload1);
	QCOMPARE(spy.at(1).at(0).toByteArray(), payload2);

	tested.stop();
}

void TestTransport::receive_brokenFrameShouldResinchronize()
{
	QByteArray payload1 = generateData(PAYLOAD_SIZE / 2);
	QByteArray payload2 = generateData(PAYLOAD_SIZE);
	QSignalSpy spy(&tested, &Transport::frameRecved);

	tested.start();
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(payload1);
	writeToDataSource(&STOP_BYTE, sizeof(STOP_BYTE));
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(payload2);
	writeToDataSource(&STOP_BYTE, sizeof(STOP_BYTE));

	// Expect Two signals
	QVERIFY(spy.count() == 1);
	QCOMPARE(spy.at(0).at(0).toByteArray(), payload2);
}

void TestTransport::receive_brokenHardFrameShouldRecynchronize()
{
	QByteArray payload1 = generateData(PAYLOAD_SIZE );
	QByteArray payload2 = generateData(PAYLOAD_SIZE);
	QSignalSpy spy(&tested, &Transport::frameRecved);

	tested.start();
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));

	writeToDataSource(payload1);

	writeToDataSource(&STOP_BYTE, sizeof(STOP_BYTE));
	writeToDataSource(&STOP_BYTE, sizeof(STOP_BYTE));
	writeToDataSource(&STOP_BYTE, sizeof(STOP_BYTE));

	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));
	writeToDataSource(&START_BYTE, sizeof(START_BYTE));

	writeToDataSource(payload2);
	writeToDataSource(&STOP_BYTE, sizeof(STOP_BYTE));

	// Expect Two signals
	QVERIFY(spy.count() == 2);
	QCOMPARE(spy.at(0).at(0).toByteArray(), payload1);
	QCOMPARE(spy.at(1).at(0).toByteArray(), payload2);
	QCOMPARE(tested.lostFrames(), uint64_t(4));
}

QTEST_APPLESS_MAIN(TestTransport)

#include "tst_testtransport.moc"
