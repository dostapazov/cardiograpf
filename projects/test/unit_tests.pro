TEMPLATE = subdirs
INCLUDEPATH += \
		   $${PWD}/../../src \
		   $${PWD}/../../libs

CONFIG += c++11

SUBDIRS += \
	paramcalc \
	transport\
	resampler\
