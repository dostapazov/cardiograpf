﻿#include "cardiographbase.hpp"
#include <QTimer>
#include <QEventLoop>
#include <QThread>
#include <QDebug>
#include <utility>

CardiographBase::CardiographBase(QObject* parent)
	: IDevice{parent},
	  mutex{QMutex::RecursionMode::Recursive}
{}

bool CardiographBase::isActive() const noexcept
{
	return active ;
}

DeviceFeatures CardiographBase::features() const noexcept
{
	DeviceFeatures ft;
	ft.inventoryNum = inventoryNumber;
	return ft;
}

void CardiographBase::setOutputDuration(int secunds) noexcept
{
	QMutexLocker l(&mutex);
	outputSecunds = secunds;
	size_t outputSize = features().sampleRate * secunds;

	for (auto& cdata : channels)
		cdata.set_capacity(outputSize);
}

void CardiographBase::setZeroOffset(uint16_t value) noexcept
{
	QMutexLocker l(&mutex);
	zeroOffset = value;
}

void CardiographBase::setInventoryNumber(const QString& inumber) noexcept
{
	QMutexLocker l(&mutex);
	inventoryNumber = inumber;
}

void  CardiographBase::clearChannels()
{
	QMutexLocker l(&mutex);
	for (auto& cdata : channels)
		cdata.clear();
	m_overflowCount = 0;
}

bool CardiographBase::start() noexcept
{
	if (isActive())
		return true;

	clearChannels();
	active = true;
	return true;
}

bool CardiographBase::stop() noexcept
{
	if (isActive())
	{
		active = false;
		clear();
	}
	return true;
}

bool CardiographBase::normalization(int duration) noexcept
{
	qInfo() << "Normalization started" << duration << " ms";
	QTimer timer;
	QEventLoop loop;
	auto c = QObject::connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);
	timer.start(duration);
	loop.exec();
	clear();
	QObject::disconnect(c);
	return true;
}

uint16_t CardiographBase::brokenChannels() const noexcept
{
	return brokenChannlesMask;
}

size_t CardiographBase::channelsCount() const noexcept
{
	return size_t(InputChannelType::CHANNELS_COUNT);
}

size_t CardiographBase::missedCount() const noexcept
{
	QMutexLocker l(&mutex);
	size_t ret = 0;
	std::swap(ret, m_overflowCount);
	return ret;
}

size_t CardiographBase::samplesCountNoLock() const noexcept
{
	if (!isActive())
		return 0;
	size_t count = std::numeric_limits<size_t>::max();
	for ( auto& channel : channels)
	{
		count = std::min(count, channel.size());
	}
	return count;
}

size_t CardiographBase::samplesCount() const noexcept
{
	QMutexLocker l(&mutex);
	return   samplesCountNoLock() ;
}

void CardiographBase::clear() noexcept
{
	QMutexLocker l(&mutex);
	for (auto& channel : channels)
	{
		channel.clear();
	}
}

size_t CardiographBase::readSignals(size_t channel, pointer_v buffer, size_t signalCount ) noexcept
{
	if (!isActive())
	{
		return 0;
	}

	QMutexLocker l(&mutex);

	Resampler::Values readValues;
	size_t sizeToRead = qMin(signalCount, samplesCountNoLock());
	queue_t& cdata = channels.at(channel);
	std::copy_n(cdata.begin(), sizeToRead, buffer);
	return sizeToRead;
}

size_t CardiographBase::removeSignals(size_t signalCount) noexcept
{
	QMutexLocker l(&mutex);

	size_t sizeToRemove = qMin(signalCount, samplesCountNoLock());

	for (queue_t& cdata : channels)
		cdata.erase_begin(sizeToRemove);

	return sizeToRemove;
}

IDevice::value_type CardiographBase::getChannelValue(CardiographBase::InputChannelType ch, const QByteArray& payload) const noexcept
{
	if (brokenChannlesMask)
		return 0;
	value_type value =  channelValue(ch, payload);
	value -= zeroOffset;
	return value;
}

void CardiographBase::addChannelValue(OutputChannelType channelNumber, IDevice::value_type value )
{
	if (size_t(channelNumber) >= size_t(InputChannelType::CHANNELS_COUNT))
		return;
	queue_t& cdata = channels.at(size_t(channelNumber));
	if (channelNumber == OutputChannelType::FirstChannel &&  cdata.full())
	{
		++m_overflowCount;
		for (auto& c : channels)
			c.erase_begin(1);
	}

	cdata.insert(cdata.end(), value);
}

bool CardiographBase::receive(const QByteArray& payload )
{
	setBrokenChannels(getBrokenChannels(payload));
	if (!isActive())
	{
		return false;
	}

	value_type RF = -getChannelValue(InputChannelType::RF, payload);
	value_type LR = getChannelValue(InputChannelType::LF, payload) + RF;

	QMutexLocker l(&mutex);

	addChannelValue(OutputChannelType::LR, LR);
	addChannelValue(OutputChannelType::RF, RF);

	for (size_t index = size_t(InputChannelType::C1F); index < size_t(InputChannelType::CHANNELS_COUNT); index++)
	{
		value_type value = getChannelValue( InputChannelType(index), payload);
		addChannelValue(OutputChannelType(index), value + RF);
	}
	return true;
}

void CardiographBase::setBrokenChannels(uint16_t channelMask) noexcept
{
	constexpr int CHANNEL_DEBOUNCE_TRIME = 300;
	if (brokenChannlesMask == channelMask)
		return;

	if (channelMask )
	{
		debouncedTimer.invalidate();
		brokenChannlesMask = channelMask;
		return;
	}

	if (!debouncedTimer.isValid())
	{
		debouncedTimer.start();
	}
	else
	{
		if (debouncedTimer.hasExpired(CHANNEL_DEBOUNCE_TRIME))
		{
			brokenChannlesMask = channelMask;
		}
	}
}

uint16_t CardiographBase::getLo4(uint8_t v) noexcept
{
	return uint16_t(v & 0x0F);
}

uint16_t CardiographBase::getHi4(uint8_t v) noexcept
{
	return uint16_t(v & 0xF0) >> 4;
}

uint16_t CardiographBase::makeValue(const uint8_t* ptr) noexcept
{
	return (getLo4(ptr[0]) << 8) | uint16_t(ptr[1]);
}
