﻿#ifndef CARDIOGRAPHBASE_H
#define CARDIOGRAPHBASE_H

#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QMutex>

#include <resampler/resampler.hpp>
#include <QElapsedTimer>
#include "idevice.hpp"
#include <boost/circular_buffer.hpp>
#include <array>

class CardiographBase : public IDevice
{
	Q_OBJECT

public:

	enum class InputChannelType : size_t
	{
		FirstChannel = 0,
		RF = FirstChannel, LF, C1F, C2F, C3F, C4F, C5F, C6F,
		CHANNELS_COUNT
	};

	enum class OutputChannelType : size_t
	{
		FirstChannel = 0,
		LR = FirstChannel, RF,  C1, C2, C3, C4, C5, C6,
		CHANNELS_COUNT
	};

	explicit CardiographBase(QObject* parent = nullptr);
	bool isActive() const noexcept override final;
	DeviceFeatures features() const noexcept override;
	void setOutputDuration(int secunds) noexcept override final;
	void setZeroOffset(uint16_t value) noexcept override final;
	void setInventoryNumber(const QString& inumber) noexcept override final;
	bool start() noexcept override;
	bool stop() noexcept override ;
	bool normalization(int duration)  noexcept override;
	uint16_t brokenChannels() const noexcept override;
	size_t channelsCount() const noexcept override;
	size_t missedCount()   const noexcept override;
	size_t samplesCount() const noexcept override;
	size_t readSignals(size_t channel, pointer_v buffer, size_t signalCount  )  noexcept override;
	size_t removeSignals(size_t signalCount) noexcept override;

public Q_SLOTS:

	bool receive(const QByteArray& payload ) override ;

protected :
	virtual value_type channelValue(InputChannelType type, const QByteArray& payload ) const noexcept  = 0;
	virtual uint16_t  getBrokenChannels(const QByteArray& payload) noexcept = 0;
	static uint16_t makeValue(const uint8_t* ptr) noexcept;
	static uint16_t getHi4(uint8_t v) noexcept;
	static uint16_t getLo4(uint8_t v) noexcept;

private:
	void setBrokenChannels(uint16_t channelMask) noexcept;
	void clear() noexcept;
	size_t samplesCountNoLock()const noexcept;

	Resampler::ValueType zeroOffset = 0x7FF;
	using queue_t = boost::circular_buffer<value_type>;
	using Channels = std::array<queue_t, size_t(InputChannelType::CHANNELS_COUNT)>;
	Channels channels;
	quint16  brokenChannlesMask = 0xFF;
	QElapsedTimer debouncedTimer;
	bool  active = false;
	QString inventoryNumber;
	size_t  outputSecunds = 3;
	mutable size_t  m_overflowCount = 0;

	mutable QMutex mutex;
	value_type getChannelValue( InputChannelType, const QByteArray& payload) const noexcept;

	void addChannelValue(OutputChannelType channelNumber, IDevice::value_type value );
	void clearChannels();
};

#endif // CARDIOGRAPHBASE_H
