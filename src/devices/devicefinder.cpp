﻿#include "devicefinder.hpp"
#include <QSerialPortInfo>
#include <QEventLoop>
#include <QRegularExpression>
#include <QDebug>

#include "modelek12t.hpp"
#include "modelkrpb_01.hpp"

DeviceFinder::DeviceFinder(QObject* parent)
	: QObject{parent}
{}

DeviceFinder::~DeviceFinder()
{
	stop();
}

void DeviceFinder::setSerialCandidateList(const QStringList& list)
{
	this->candidatePortNames = list;
}

void DeviceFinder::start()
{
	qInfo() << "Start device finder";
	watchDogTimer.reset( new QTimer);
	watchDogTimer->setSingleShot(true);
	connect(watchDogTimer.data(), &QTimer::timeout, this, &DeviceFinder::findDevice);
	watchDogTimer->start(1);
}

void DeviceFinder::stop()
{
	if (!watchDogTimer)
		return;

	qInfo() << "Stop device finder";

	watchDogTimer.reset(nullptr);
	serialPort.reset(nullptr);
	transport.reset(nullptr);
	device.reset(nullptr);
}

bool DeviceFinder::isPortCandidate(const QString& portName) const
{
	QRegularExpression regExp;

	for (const QString& name : candidatePortNames)
	{
		regExp.setPattern(name);
		if (regExp.match(portName).hasMatch())
			return true;
	}
	return false;
}

QStringList DeviceFinder::makePortNameList()
{
	QStringList result;
	for ( auto&& portInfo : QSerialPortInfo::availablePorts())
	{
		if (portInfo.isNull())
		{
			continue;
		}

		if (isPortCandidate(portInfo.portName()))
		{
			result.append(portInfo.portName());
		}
	}
	return result;
}

void DeviceFinder::setDevice(IDevice* dev)
{
	if (dev != device.data())
	{
		device.reset(dev);
		emit deviceFound(device.data(), QPrivateSignal());
		if (dev)
		{
			connect(transport.data(), &Transport::frameRecved, dev, &IDevice::receive, Qt::QueuedConnection) ;
		}
	}
}

void DeviceFinder::findDevice()
{
	watchDogTimer->stop();

	setDevice(nullptr);
	transport.reset(nullptr);
	serialPort.reset(nullptr);

	const QStringList portNames = makePortNameList();
	QStringList::const_iterator end = portNames.cend();
	QStringList::const_iterator ptr = portNames.cbegin();
	ptr = std::find_if(ptr, end, [this](const QString & portName) {return testPort(portName);});
	if (ptr < end)
	{
		return;
	}

	watchDogTimer->start(WATCHDOG_TIMEOUT);
}

void DeviceFinder::createSerialPort(const QString& portName)
{
	if (!serialPort)
	{
		serialPort.reset(new QSerialPort);
	}
	serialPort->close();
	serialPort->setPortName(portName);
	serialPort->setBaudRate(QSerialPort::BaudRate::Baud115200);
	serialPort->setDataBits(QSerialPort::Data8);
	serialPort->setParity(QSerialPort::NoParity);
	serialPort->setFlowControl(QSerialPort::FlowControl::NoFlowControl);
}

bool DeviceFinder::testPort(const QString& portName)
{
	createSerialPort(portName);
	if (!serialPort->open(QIODevice::ReadWrite))
		return false;

	serialPort->clearError();
	serialPort->clear();

	if (serialPort->waitForReadyRead(200))
	{
		if (testTransport())
		{
			return true;
		}
	}

	serialPort->close();
	return false;
}

bool DeviceFinder::waitTransportRespond(const TransportParams& tp)
{
	QTimer     waitFrameTimer;
	QEventLoop loop;
	waitFrameTimer.setSingleShot(true);
	waitFrameTimer.setInterval(300);

	connect(transport.data(), &Transport::frameRecved, &loop, &QEventLoop::quit);
	connect(&waitFrameTimer, &QTimer::timeout, &loop, [&loop] {loop.exit(-1); });

	transport->setConfig(Transport::Config(tp.startMarker, tp.stopMarker, tp.payloadSize));
	transport->start();

	waitFrameTimer.start();
	int res = loop.exec();
	waitFrameTimer.stop();

	if (res)
	{
		transport->stop();
		return false;
	}
	return true;
}

void DeviceFinder::transportWatchdog()
{
	watchDogTimer->stop();
	watchDogTimer->start(WATCHDOG_TIMEOUT);
}

bool DeviceFinder::testTransport()
{
	if (!transport)
	{
		transport.reset(new Transport);
		connect(transport.data(), &Transport::frameRecved, this, &DeviceFinder::transportWatchdog);
	}

	transport->setIoDevice(serialPort.data());

	if (waitTransportRespond(TRANSPORT_PARAMS_EK_12T))
	{
		setDevice(new ModelEK12T);
		return true;
	}

	if (waitTransportRespond(TRANSPORT_PARAMS_KRP))
	{
		setDevice(new ModelKRPB_01);
		return true;
	}
	return false;
}

QString DeviceFinder::getCurrentPortNname() const
{
	return serialPort.data() ? serialPort->portName() : QString();
}
