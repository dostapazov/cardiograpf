﻿#ifndef DEVICEFINDER_HPP

#define DEVICEFINDER_HPP

#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QSerialPort>
#include <QThread>
#include <QTimer>

#include <transport/transport.hpp>
#include <devices/idevice.hpp>

class DeviceFinder : public QObject
{
	Q_OBJECT

	static constexpr int WATCHDOG_TIMEOUT = 1000;

	QStringList candidatePortNames;
	QScopedPointer<QTimer, QScopedPointerDeleteLater>  watchDogTimer;
	QScopedPointer<QSerialPort, QScopedPointerDeleteLater>  serialPort;

	QScopedPointer<Transport, QScopedPointerDeleteLater> transport;
	QScopedPointer<IDevice, QScopedPointerDeleteLater> device;

public:
	explicit DeviceFinder(QObject* parent = nullptr);
	~DeviceFinder() override;
	void setSerialCandidateList(const QStringList& list);
	void start();
	void stop();
	QString getCurrentPortNname() const;

signals:
	void deviceFound(IDevice* dev, QPrivateSignal);

private Q_SLOTS:
	void findDevice();
	void transportWatchdog();
private:
	bool testPort(const QString& portName);
	QStringList makePortNameList();
	void createSerialPort(const QString& portName);
	void setDevice(IDevice* dev);
	bool isPortCandidate(const QString& portName) const;
	bool testTransport();
	bool waitTransportRespond(const TransportParams& tp);
};

#endif // DEVICEFINDER_HPP
