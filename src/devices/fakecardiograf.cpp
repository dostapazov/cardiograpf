﻿#include "fakecardiograf.hpp"
#include <QThread>
#include <QRandomGenerator>

FakeCardiograf::FakeCardiograf(QObject* parent)
	: IDevice{parent}
{
}

void FakeCardiograf::setInventoryNumber(const QString& inumber)
{
	inventoryNum = inumber;
}

DeviceFeatures FakeCardiograf::features() const
{
	DeviceFeatures df;
	df.inventoryNum = inventoryNum;
	df.model = "FakeModel";
	df.version = "1.0";
	df.sampleRate = 500;
	df.resolution = 5.0;

	return df;
}

TransportParams FakeCardiograf::transportParams()
{
	return {'{', '}', 16};
}

void FakeCardiograf::setOutputDuration(int secunds)
{
	Q_UNUSED(secunds);
}

void FakeCardiograf::generateData(int points, double freq,  double ampl, double offs )
{
	double inc = double(fabs(freq) * 2.0 * M_PI) / double(points);
	double start = .0;
	bool neg = freq < .0;

	genData.resize(points);

	std::generate(genData.begin(), genData.end(),
				  [ =, &start ]() ->uint16_t
	{
		double val = neg ? cos(start) : sin(start);
		start += inc;
		return uint16_t(offs + val * ampl);
	}
				 );


}


bool FakeCardiograf::start(int sampleRate, double resolution)
{
	Q_UNUSED (sampleRate)
	Q_UNUSED (resolution)
	generateData(sampleRate, 25, 100, 100);
	QMutexLocker l(&mut);
	framesCount = 0;
	readPosition = 0;

	return (sampleRate >= 250  && resolution > 0.1);
}

bool FakeCardiograf::stop()
{
	return true;
}

bool FakeCardiograf::normalization(int duration)
{
	QThread::msleep(duration);
	return true;
}

uint16_t FakeCardiograf::brokenChannels()
{
	return 0x00;
}

size_t FakeCardiograf::channelsCount()
{
	return 8;
}

size_t FakeCardiograf::missedCount()
{
	return 0;
}

size_t FakeCardiograf::samplesCount()
{
	QMutexLocker l(&mut);
	return framesCount;
}

QByteArray FakeCardiograf::readSignals( size_t size )
{
	QByteArray samples;
	size_t rdSize = qMin(size, samplesCount());

	QByteArray data;
	data.resize(rdSize * sizeof(uint16_t));
	uint16_t* dptr = reinterpret_cast<uint16_t*>(data.data());
	for (size_t idx = 0; idx < rdSize; ++idx)
	{
		dptr[0] = genData.at(readPosition);
		++dptr;
		readPosition = (readPosition + 1) % genData.size();
	}

	for (size_t i = 0; i < channelsCount(); i++)
	{
		samples.append(data);
	}
	return samples;
}

void FakeCardiograf::receive(const QByteArray& payload)
{
	Q_UNUSED(payload);
	QMutexLocker l(&mut);
	++framesCount;
}



