﻿#ifndef FAKECARDIOGRAF_HPP
#define FAKECARDIOGRAF_HPP

#include "idevice.hpp"
#include <QMutex>

class FakeCardiograf : public IDevice
{
	Q_OBJECT
public:
	explicit FakeCardiograf(QObject* parent = nullptr);

	void setInventoryNumber(const QString& inumber) override ;
	DeviceFeatures features() const override final;
	TransportParams transportParams() override final;
	void setOutputDuration(int secunds) override final;
	bool start(int sampleRate, double resolution) override;
	bool stop() override;
	bool normalization(int duration) override;
	uint16_t brokenChannels() override;
	size_t channelsCount() override;
	size_t missedCount() override;
	size_t samplesCount() override;
	QByteArray readSignals(size_t size = -1) override;

public Q_SLOTS:
	virtual void receive(const QByteArray& payload ) override;

private:
	QString inventoryNum = "666";
	QVector<uint16_t> genData;
	size_t framesCount = 0;
	size_t readPosition = 0;
	QMutex mut;
	void generateData(int points, double freq,  double ampl, double offs );

};

#endif // FAKECARDIOGRAF_HPP
