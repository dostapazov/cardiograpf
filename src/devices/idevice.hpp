﻿#ifndef IDEVICE_HPP
#define IDEVICE_HPP

#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QVector>

#include <QString>

struct DeviceFeatures
{
	QString inventoryNum;
	QString model;
	QString version;
	unsigned int sampleRate = 500;
	double  resolution = 5.0;
};

struct TransportParams
{
	uint8_t startMarker ;
	uint8_t stopMarker ;
	uint16_t payloadSize;
} ;

class IDevice : public QObject
{
	Q_OBJECT
public:
	using value_type = int16_t;
	using pointer_v = value_type*;
	using const_pointer_v = const value_type*;

	explicit IDevice(QObject* parent = nullptr): QObject{parent} {}
	virtual void setInventoryNumber(const QString& inumber) = 0;

	virtual bool isActive() const noexcept = 0;
	virtual DeviceFeatures features() const noexcept = 0;
	virtual TransportParams transportParams() const noexcept = 0;
	virtual void setOutputDuration(int secunds) noexcept = 0;
	virtual void setZeroOffset(uint16_t value) noexcept = 0;
	virtual bool start() noexcept = 0;
	virtual bool stop() noexcept = 0;
	virtual bool normalization(int duration) noexcept = 0;
	virtual uint16_t brokenChannels() const noexcept = 0;
	virtual size_t channelsCount() const noexcept = 0;
	virtual size_t missedCount()  const noexcept = 0;
	virtual size_t samplesCount() const noexcept = 0;
	virtual size_t readSignals(size_t channel, pointer_v buffer, size_t signalCount) noexcept = 0;
	virtual size_t removeSignals(size_t signalCount) noexcept = 0;
public Q_SLOTS:
	virtual bool receive(const QByteArray& payload )  = 0;

};

#endif // IDEVICE_HPP
