﻿#include "modelek12t.hpp"
#include <QDebug>


ModelEK12T::ModelEK12T(QObject* parent ) :
	CardiographBase{parent}
{};

TransportParams ModelEK12T::transportParams() const noexcept
{
	return TRANSPORT_PARAMS_EK_12T;
}

DeviceFeatures ModelEK12T::features() const noexcept
{
	DeviceFeatures features = CardiographBase::features();
	features.model = QString::fromStdWString(MODEL_EK_12T);
	features.version = QString::asprintf("%02d", int(version));
	features.sampleRate = version == 1 ? SAMPLE_RATE_V1 : SAMPLE_RATE_V0;
	features.resolution = RESOLUTION;
	return features;
}

bool ModelEK12T::receive(const QByteArray& payload ) noexcept
{
	if (isActive())
	{
		const uint8_t* uptr = reinterpret_cast<const uint8_t*>(payload.constData());
		getVersions(uptr);
	}
	return CardiographBase::receive(payload);
}

constexpr size_t getUOffset(size_t number)
{
	return (number ) << 1;
}

enum  C_OFFSETS : size_t
{
	RF_OFFS = 5,
	LF_OFFS = 7,
	C1_OFFS = 1,
	C2_OFFS = 4,
	C3_OFFS = 6,
	C4_OFFS = 3,
	C5_OFFS = 2,
	C6_OFFS = 0
};

void ModelEK12T::getVersions(const uint8_t* uptr)
{
	constexpr size_t VER_OFFSET = 2;
	version = getHi4( uptr[VER_OFFSET]);
}

ModelEK12T::ValueType ModelEK12T::channelValue(CardiographBase::InputChannelType type, const QByteArray& payload ) const noexcept
{
	const uint8_t* uptr = reinterpret_cast<const uint8_t*>(payload.constData());
	switch (type)
	{
		case InputChannelType::RF:
			uptr += getUOffset(C_OFFSETS::RF_OFFS);
			break;
		case InputChannelType::LF:
			uptr += getUOffset(C_OFFSETS::LF_OFFS);
			break;
		case InputChannelType::C1F:
			uptr += getUOffset(C_OFFSETS::C1_OFFS);
			break;
		case InputChannelType::C2F:
			uptr += getUOffset(C_OFFSETS::C2_OFFS);
			break;
		case InputChannelType::C3F:
			uptr += getUOffset(C_OFFSETS::C3_OFFS);
			break;
		case InputChannelType::C4F:
			uptr += getUOffset(C_OFFSETS::C4_OFFS);
			break;
		case InputChannelType::C5F:
			uptr += getUOffset(C_OFFSETS::C5_OFFS);
			break;
		case InputChannelType::C6F:
			uptr += getUOffset(C_OFFSETS::C6_OFFS);
			break;
		default :
			break;
	}

	return makeValue(uptr);
}

uint16_t  ModelEK12T::getBrokenChannelsV0(const uint8_t* uptr) const
{
	uint8_t LOFF = getHi4(uptr[0]);

	if (LOFF == 0x0F)
		return 0;

	if (LOFF > 7)
		return 0xFF;

	constexpr uint16_t brokeBit [] =
	{
		uint16_t(InputChannelType::C3F), uint16_t(InputChannelType::C4F), uint16_t(InputChannelType::C5F),
		uint16_t(InputChannelType::C1F), uint16_t(InputChannelType::C2F), uint16_t(InputChannelType::RF),
		uint16_t(InputChannelType::C6F), uint16_t(InputChannelType::LF)
	};
	uint16_t channelMask = 1 << brokeBit[LOFF];
	return channelMask;
}

uint16_t  ModelEK12T::getBrokenChannelsV1(const uint8_t* uptr) const
{
	uint8_t LOM  = (uptr[6] & 0xF0) | getHi4(uptr[4]);
	return LOM;
}

uint16_t  ModelEK12T::getBrokenChannels(const QByteArray& payload) noexcept
{
	if (payload.size() != 16)
		return 0xFF;

	const uint8_t* uptr = reinterpret_cast<const uint8_t*>(payload.constData());

	if (version == 0)
	{
		return getBrokenChannelsV0(uptr);
	}

	return getBrokenChannelsV1(uptr);
}
