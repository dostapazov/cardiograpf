﻿#ifndef MODELEK12T_H
#define MODELEK12T_H

#include "cardiographbase.hpp"

constexpr const wchar_t* MODEL_EK_12T = L"ЭК12T-01";

constexpr TransportParams TRANSPORT_PARAMS_EK_12T = {'<', '>', 16};

class ModelEK12T : public CardiographBase
{
	Q_OBJECT
public:
	explicit ModelEK12T(QObject* parent = nullptr);
	DeviceFeatures features() const noexcept override;

	TransportParams transportParams() const noexcept override;
public Q_SLOTS:
	bool receive(const QByteArray& payload ) noexcept override final;

private:

	using ValueType = Resampler::ValueType;
	ValueType channelValue(CardiographBase::InputChannelType type, const QByteArray& payload ) const noexcept override;
	uint16_t  getBrokenChannels(const QByteArray& payload) noexcept override final;
	uint16_t  version = 0;

	static constexpr unsigned int SAMPLE_RATE_V1 = 500;
	static constexpr unsigned int SAMPLE_RATE_V0 = 400;
	static constexpr double RESOLUTION = 4.78;

	void getVersions(const uint8_t* uptr);
	uint16_t getBrokenChannelsV0(const uint8_t* uptr) const;
	uint16_t getBrokenChannelsV1(const uint8_t* uptr) const;
};

#endif // MODELEK12T_H
