﻿#include "modelkrpb_01.hpp"

ModelKRPB_01::ModelKRPB_01(QObject* parent)
	: CardiographBase{parent}
{
}

TransportParams ModelKRPB_01::transportParams() const noexcept
{
	return TRANSPORT_PARAMS_KRP;
}

DeviceFeatures ModelKRPB_01::features() const noexcept
{
	DeviceFeatures ft = CardiographBase::features();
	ft.model = QString::fromStdWString((!getDeviceType()  ? MODEK_KRP_01 : MODEL_KRB_01));
	ft.resolution = RESOLUTION;
	ft.sampleRate = SAMPLE_RATE;
	ft.version = QString::asprintf("%02X:%02X", SWW, ProtoVersion);

	return ft;
}

bool ModelKRPB_01::receive(const QByteArray& payload )
{

	getSpecificData(payload);
	return CardiographBase::receive(payload);
}

bool     ModelKRPB_01::getPMM() const
{
	return DevPM & 0x01;
}

bool     ModelKRPB_01::getPMI() const
{
	return DevPM & 0x02;
}

uint16_t ModelKRPB_01::getDeviceType() const
{
	return (DevPM >> 2) & 0x03;
}

void ModelKRPB_01::getSpecificData(const QByteArray& payload)
{
	constexpr size_t  SWW_OFFSET = 0;
	constexpr size_t  PROTO_VER_OFFSET = 2;
	constexpr size_t  AL_LO_OFFSET = 8;
	constexpr size_t  AL_HI_OFFSET = AL_LO_OFFSET + 2;
	constexpr size_t  DEV_PM_OFFSET = 12;
	constexpr size_t  FLT_OFFSET = 14;

	const uint8_t* uptr = reinterpret_cast<const uint8_t*>(payload.constData());
	SWW = getHi4(uptr[SWW_OFFSET]);
	ProtoVersion = getHi4(uptr[PROTO_VER_OFFSET]);
	AL =  uint16_t(uptr[AL_HI_OFFSET] & 0xF0 ) | getHi4(uptr[AL_LO_OFFSET] );
	DevPM = getHi4(uptr[DEV_PM_OFFSET]);
	Filters = getHi4(uptr[FLT_OFFSET]) & 0x07;
}

ModelKRPB_01::ValueType ModelKRPB_01::channelValue(CardiographBase::InputChannelType type, const QByteArray& payload ) const noexcept
{
	const uint8_t* uptr = reinterpret_cast<const uint8_t*>(payload.constData());
	switch (type)
	{
		case InputChannelType::RF:
			uptr += 0;
			break;
		case InputChannelType::LF:
			uptr += 2;
			break;
		case InputChannelType::C1F:
			uptr += 4;
			break;
		case InputChannelType::C2F:
			uptr += 6;
			break;
		case InputChannelType::C3F:
			uptr += 8;
			break;
		case InputChannelType::C4F:
			uptr += 10;
			break;
		case InputChannelType::C5F:
			uptr += 12;
			break;
		case InputChannelType::C6F:
			uptr += 14;
			break;
		default :
			break;
	}

	return makeValue(uptr);
}

uint16_t  ModelKRPB_01::getBrokenChannels(const QByteArray& payload) noexcept
{
	constexpr size_t LOM_LO_OFFSET = 4;
	constexpr size_t LOM_HI_OFFSET = 6;
	const uint8_t* uptr = reinterpret_cast<const uint8_t*>(payload.constData());
	return (uptr[LOM_HI_OFFSET] & 0xF0) | getHi4(uptr[LOM_LO_OFFSET]);
}
