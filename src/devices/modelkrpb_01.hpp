﻿#ifndef MODELKRPB_01_HPP
#define MODELKRPB_01_HPP


#include "cardiographbase.hpp"

constexpr const wchar_t* MODEK_KRP_01 = L"КРП-01";
constexpr const wchar_t* MODEL_KRB_01 = L"КРБ-01";
constexpr const TransportParams TRANSPORT_PARAMS_KRP = {'{', '}', 16};

class ModelKRPB_01 : public CardiographBase
{
	Q_OBJECT
public:
	explicit ModelKRPB_01(QObject* parent = nullptr);
	DeviceFeatures features() const noexcept override;

	TransportParams transportParams() const noexcept override;
public Q_SLOTS:
	bool receive(const QByteArray& payload ) override final;

private:
	using ValueType = Resampler::ValueType;
	ValueType channelValue(CardiographBase::InputChannelType type, const QByteArray& payload ) const noexcept override;
	uint16_t  getBrokenChannels(const QByteArray& payload) noexcept override final;
	void getSpecificData(const QByteArray& payload);
	uint16_t getDeviceType() const;
	bool     getPMM() const;
	bool     getPMI() const;

	static constexpr unsigned int SAMPLE_RATE = 500;
	static constexpr double RESOLUTION = 4.78;

	uint16_t version = 0;
	uint16_t SWW = 0;
	uint16_t ProtoVersion = 0;
	uint16_t AL = 0;
	uint16_t DevPM = 0;
	uint16_t Filters = 0;
};

#endif // MODELKRPB_01_HPP
