﻿#include <math.h>
#include <memory.h>
#include "resampleimpl.hpp"

namespace resampleimpl {

template <typename Real>
inline void fillArray(ArrayOfReal<Real>& c, Real value = 0)
{
	std::fill(c.begin(), c.end(), value);
}

template <typename Real>
inline void clearArray(ArrayOfReal<Real>& c)
{
	fillArray(c, .0);
}

template <typename Real>
static void polyval(const ArrayOfReal<Real>& a, int ord, const Real* x, int n, Real* y)
{
	for (int k = 0; k < n; k++)
	{
		y[k] = a[ord];
		for (int m = ord - 1; m > -1; m--)
			y[k] = y[k] * x[k] + a[m];
	}
}

static size_t calcNewSize( size_t inSize,  size_t toSamples,  size_t fromSamples)
{
	double tmp = double( (inSize - 1) * toSamples) / double(fromSamples);
	size_t nsz = static_cast<size_t>(tmp);
	return nsz + 1;
}

template <typename Real>
bool checkInputConditions(size_t iSize, size_t toSamples, size_t fromSamples, Real frd)
{
	return (iSize >= 1) &&
		   (toSamples > 0 && fromSamples > 0) &&
		   (frd > -1.0 && frd < 1.0)
		   ;
}

template <typename Real>
void calcLagrange4(const Real* input, ArrayOfReal<Real>& a )
{
	constexpr Real DSPL_FARROW_LAGRANGE_COEFF =  0.16666666666666666666666666666667;
	a[0] = input[2];
	a[3] = DSPL_FARROW_LAGRANGE_COEFF * (input[3] - input[0]) + 0.5 * (input[1] - input[2]);
	a[1] = 0.5 * (input[3] - input[1]) - a[3];
	a[2] = input[3] - input[2] - a[3] - a[1];
}

template <typename Real>
void calcSpline4(const Real* input, ArrayOfReal<Real>& a )
{
	a[0] = input[2];
	a[1] = 0.5 * (input[3] - input[1]);
	a[3] = 2.0 * (input[1] - input[2]) + a[1] + 0.5 * (input[2] - input[0]);
	a[2] = input[1] - input[2] + a[3] + a[1];
}

template <typename Real>
bool resample(const CalcPolynom<Real>& calcPolynom,
			  const VectorOfReal<Real>& src,
			  VectorOfReal<Real>& dst,
			  size_t toSamples, size_t fromSamples, Real frd )

{
	if (!checkInputConditions(src.size(), toSamples, fromSamples, frd))
		return false;

	constexpr size_t ORDER = 4;

	Real inFront = src.front();
	Real inBack = src.back();

	Real dt = double(fromSamples) / double(toSamples);
	int newSize = calcNewSize(src.size(), toSamples, fromSamples);
	dst.resize(newSize);

	double t = -frd;
	auto dptr = dst.begin();
	auto dend = dst.end();

	while (dptr < dend)
	{
		const Real* input;
		ArrayOfReal<Real> g;
		int ind = (int)floor(t) + 1;
		Real x = t - (double)ind;
		ind -= 2;
		if (ind < 0)
		{
			input = g.data();
			g.front() = inFront;

			if (ind > (-3))
			{
				memcpy(g.data() - ind, src.data(), (ORDER + ind)*sizeof(Real));
			}
		}
		else
		{
			if (ind < int(src.size()) - 3)
				input = src.data() + ind;
			else
			{
				input = g.data();
				g.back() = inBack;
				if ((src.size() - ind) > 0)
				{
					memcpy(g.data(), src.data() + ind, (src.size() - ind)*sizeof(Real));
				}
			}
		}

		ArrayOfReal<Real> a;
		calcPolynom(input, a);
		polyval(a, 3, &x, 1, dptr.base());

		t += dt;
		++dptr;
	}

	return true;
}

bool lagrange(const VectorOfDouble& src, VectorOfDouble& dst, size_t toSamples, size_t fromSamples,
			  double frd )
{
	return resample<double>(calcLagrange4<double>, src, dst, toSamples, fromSamples, frd);
}

bool lagrange(const VectorOfFloat& src, VectorOfFloat& dst, size_t toSamples, size_t fromSamples,
			  float frd )
{
	return resample<float>(calcLagrange4<float>, src, dst, toSamples, fromSamples, frd);
}

bool spline(const VectorOfFloat& src, VectorOfFloat&& dst, size_t toSamples, size_t fromSamples,
			float frd )
{
	return resample<float>(&calcSpline4<float>, src, dst, toSamples, fromSamples, frd);
}

bool spline(const VectorOfDouble& src, VectorOfDouble& dst, size_t toSamples, size_t fromSamples,
			double frd )
{
	return resample<double>(&calcSpline4<double>, src, dst, toSamples, fromSamples, frd);
}

}
