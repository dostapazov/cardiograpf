﻿#ifndef RESAMPLEIMPL_HPP
#define RESAMPLEIMPL_HPP

#include <vector>
#include <stddef.h>
#include <functional>
#include <array>


namespace resampleimpl {

template<typename Real>
using VectorOfReal = std::vector<Real>;


template<typename Real, size_t ORDER = 4>
using ArrayOfReal = std::array<Real, ORDER>;


template<typename Real>
using CalcPolynom = std::function<void(const Real*, ArrayOfReal<Real>&)>;


using VectorOfDouble = VectorOfReal<double>;

bool lagrange(const VectorOfDouble& src, VectorOfDouble& dst, size_t toSamples, size_t fromSamples,
			  double frd = .0);

bool spline(const VectorOfDouble& src, VectorOfDouble& dst, size_t toSamples, size_t fromSamples,
			double frd = .0);

using VectorOfFloat = VectorOfReal<float>;

bool lagrange(const VectorOfFloat& src, VectorOfFloat& dst, size_t toSamples, size_t fromSamples,
			  float frd = .0);

bool spline(const VectorOfFloat& src, VectorOfFloat& dst, size_t toSamples, size_t fromSamples,
			float frd = .0);


};

#endif // RESAMPLEIMPL_HPP
