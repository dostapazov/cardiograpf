﻿#include "resampler.hpp"
#include <cmath>
#include <algorithm>

bool Resampler::isNull(double v)
{
	constexpr double EPSILON = .00000000001;
	return isEqual(v, .0, EPSILON);
}

bool   Resampler::isEqual(double v1, double v2, double epsilon)
{
	double delta = std::fabs(v1 - v2);
	if ( delta < epsilon)
		return true;
	return false;
}

double Resampler::recalc(Resampler::ValueType in, double coeff)
{
	if (isEqual(coeff, 1.0, .000001))
		return double(in);

	constexpr long MULTIPLIER = 10;
	double value = coeff * double(MULTIPLIER * in);
	ldiv_t dt = ldiv(long(value), MULTIPLIER);
	if (dt.rem >= 5)
		++dt.quot;

	return double(dt.quot);
}

void Resampler::updateResolutionCoefficient()
{
	if (isNull(inResolution) || isNull(outResolution))
	{
		resolutionCoefficient = .0;
	}
	else
	{
		resolutionCoefficient = inResolution / outResolution;
	}

	size_t v1 = inSamples;
	size_t v2 = outSamples;
	while (v1 > 100 && !(v1 % 10) && v2 > 100 && !(v2 % 10) )
	{
		v1 /= 10;
		v2 /= 10;
	}

	inSamplesThreshold  = v1;
	outSamplesThreshold = v2;
}


void Resampler::setInputParameters(std::size_t samples, double res)
{
	inSamples = samples;
	inResolution = res;
	updateResolutionCoefficient();
}

void Resampler::setOutputParameters(std::size_t samples, double res)
{
	outSamples = samples;
	outResolution = res;
	updateResolutionCoefficient();
}

size_t Resampler::getInputThreshold() const
{
	return inSamplesThreshold;
}


bool Resampler::needToResample() const
{
	return inSamples != outSamples;
}

void Resampler::resample(const Values& input, Values& output) const
{
	InternalData internalInput(input.size());
	InternalData internalOutput;

	auto trv = [this](ValueType v)->double
	{
		return recalc(v, resolutionCoefficient);
	};

	std::transform(input.begin(), input.end(), internalInput.begin(), trv);

	if (needToResample())
	{
		doResample(internalInput, internalOutput);
	}
	else
	{
		internalOutput = std::move(internalInput);
	}

	size_t sizeToCopy = internalOutput.size() - needToResample();
	output.resize(sizeToCopy);
	std::copy_n(internalOutput.begin(), sizeToCopy, output.begin());
	return;
}

void Resampler::doResample(const InternalData& input, InternalData& output ) const
{
	if (resampleMode == ResampleMode::Lagrange)
		resampleimpl::lagrange(input, output, outSamplesThreshold, inSamplesThreshold);
	else
		resampleimpl::spline(input, output, outSamplesThreshold, inSamplesThreshold);

}


