﻿#ifndef RESAMPLER_HPP
#define RESAMPLER_HPP

#include <vector>
#include <stdint.h>
#include <stddef.h>
#include "resampleimpl.hpp"


class Resampler
{
public:
	using ValueType = int16_t;
	using Values = std::vector<ValueType>;
	enum class ResampleMode {Lagrange, Spline};
	Resampler() = default;
	size_t getInputThreshold() const;
	size_t inputSamples() const {return inSamples;}
	double inputResolution() const { return inResolution;}

	bool needToResample() const;
	void setResampleMode(ResampleMode mode);
	void setInputParameters(size_t samples, double res);
	void setOutputParameters(size_t samples, double res);
	void resample(const Values& input, Values& output) const;

	static bool isEqual(double v1, double v2, double epsilon = 0.01);
	static bool isNull(double v);
	static double recalc(Resampler::ValueType in, double coeff);

protected:
	using  InternalData = resampleimpl::VectorOfDouble;
private:
	void updateResolutionCoefficient();

private:
	size_t inSamples = 0;
	double inResolution = .0;
	size_t outSamples = 0;
	double outResolution = .0;
	double resolutionCoefficient = .0;

	size_t inSamplesThreshold  = 0;
	size_t outSamplesThreshold = 0;


	ResampleMode resampleMode = ResampleMode::Lagrange;
	void doResample(const InternalData& input, InternalData& output) const;
};

inline void Resampler::setResampleMode(Resampler::ResampleMode mode)
{
	resampleMode = mode;
}

#endif // RESAMPLER_HPP
