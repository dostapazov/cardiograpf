﻿#ifndef HTTPAPI_HPP
#define HTTPAPI_HPP

namespace http_api {

constexpr const char API_VER_KEY[] = "accept";
constexpr const char API_VER_1_0_0[] = "1.0.0";

constexpr const char PATH_DEVICE[] = "/device";
constexpr const char PATH_SESSION[] = "/session";
constexpr const char PATH_SESSION_ACTIONS[] = "/session/<string>";
constexpr const char PATH_SESSION_DEVICE_SIGNAL[] = "/session/<string>/signal";
constexpr const char PATH_SESSION_DEVICE_NORMALIZATION[] = "/session/<string>/normalization";
constexpr const char PATH_SESSION_DEVICE_BROKENCHANNELS[] = "/session/<string>/brokenchannels";

constexpr const char DEV_FEATURES[] = "deviceFeatures";
constexpr const char DEV_INVENTORY[] = "inventoryNum";
constexpr const char DEV_MODEL [] = "model";
constexpr const char DEV_VERSION[] = "version";
constexpr const char DEV_DIGIT_RES[] = "digitResolution";
constexpr const char DEV_SAMPLE_RATE[] = "sampleRate";

constexpr const char SESSION[] = "session";
constexpr const char SESSION_EXPIRY[] = "sessionExpiry";
constexpr const char SESSION_LENGTH[] = "sessionLength";
constexpr const char SESSION_ID[] = "sessionId";

constexpr const char NORMALIZATION_INTERVAL[] = "interval";

constexpr const char BROKEN_CHANNELS[] = "brokenChannels";
constexpr const char BROKEN_CHANNELS_SEPARATOR[] = "^";

constexpr const char SIGNAL_READ_RESOLUTION[] = "outDigitResolution";
constexpr const char SIGNAL_READ_SAMPLE_RATE[] = "outSampleRate";

constexpr const char SIGNAL_READ_SIZE[] = "size";
constexpr const char SIGNAL_READ_MISSED_SIZE[] = "missedSize";
constexpr const char SIGNAL_READ_DATA[] = "signals";
constexpr const char SIGNAL_READ_IS_IVR[] = "isIVR";
}

#endif // HTTPAPI_HPP
