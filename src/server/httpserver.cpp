﻿#include <QtConcurrent>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QtEndian>
#include <QDebug>

#include <functional>
#include <crow/middleware_context.h>

#include "httpserver.hpp"
#include "http-api.hpp"


QList<int> HttpServer::enabledResolution = {1, 2, 3, 4, 5};
QList<int> HttpServer::enabledSampleRate = {250, 500, 1000};

/**
 * @brief getIntegerValue
 * @param value
 * @return integer value
 * @details this function helps to convert variant to integer
 * in case string then try get integer from string
 * finally checks for strict integer representation
 */

int getIntegerValue(const QVariant& value)
{
	bool ok{false};
	double result = value.toDouble(&ok);
	if (!ok)
	{
		QString text = value.toString();
		text.remove("'");
		text.remove("\"");
		result = text.toDouble(&ok);
	}

	return (ok && qFuzzyCompare(ceil(result), floor(result))) ? int(result) : 0;
}

void HttpServer::HeadersMiddleware::before_handle(crow::request& req, crow::response& res, context& ctx)
{
	(void)req;
	(void)res;
	(void)ctx;
	if (!owner->checkApiVersion(req))
	{
		res.code = int(HttpServer::RespondCodes::InvalidParameter);
		return res.end();
	}

	if (owner->crowFuture.isCanceled())
	{
		qInfo() << "Future canceled";
		res.code = int(HttpServer::RespondCodes::DeviceBusy);
		return res.end();
	}
}

void HttpServer::HeadersMiddleware::after_handle(crow::request& req, crow::response& res, context& ctx)
{
	(void)req;
	(void)res;
	(void)ctx;

	res.add_header(http_api::API_VER_KEY, http_api::API_VER_1_0_0);

	res.add_header("Access-Control-Allow-Headers", "content-type");
	res.add_header("Access-Control-Allow-Origin", "*");
	res.add_header("Content-type", "text/json");

	res.add_header("Access-Control-Allow-Headers",
				   "Authorization, Origin, X-Requested-With, x_remote_user, Content-Type, Accept"
				   ", Kpi-Available-Resource-Id, Kpi-Complex-Resource-Id, Kpi-Contract-Id"
				   ", Kpi-Lpu-Id, Kpi-Role-Id, Kpi-User-Token, Kpi-User-Rights"
				  );

	res.add_header("Cache-Control", "no-cache, no-store, must-revalidate");
	res.add_header("Pragma", "no-cache");
	if (owner && owner->_isSessionActive())
	{
		res.add_header("Expires", owner->_sessionExpiredTimestamp().toStdString());
	}
}

HttpServer& HttpServer::instance()
{
	static HttpServer srv;
	return srv;
}

HttpServer::HttpServer(QObject* parent)
	: QObject{parent},
	  mutex(QMutex::RecursionMode::Recursive)
{
	crowApp.server_name(getServerName().toStdString());
	crowApp.get_middleware<HttpServer::HeadersMiddleware>().owner = this;
	initCorsRules();
	initHandlers();
	initOptionsHandlers();
	CROW_CATCHALL_ROUTE(crowApp)(HttpServer::invalidRequestHandler);
	connect(&sessionTimer, &QTimer::timeout, this, &HttpServer::sessionExpired);
}

void HttpServer::setDevice(IDevice* dev)
{
	if (dev == device)
		return ;

	QMutexLocker l(&mutex);
	device = dev;
	if (device)
	{
		DeviceFeatures ft = device->features();
		resampler.setInputParameters(ft.sampleRate, ft.resolution);
	}
}

void HttpServer::initCorsRules()
{
	auto& cors = crowApp.get_middleware<crow::CORSHandler>();
	cors.global().allow_credentials();
	auto& methods = cors.global().headers("X-Custom-Header", "Upgrade-Insecure-Requests")
					.methods(crow::HTTPMethod::Post, crow::HTTPMethod::Put, crow::HTTPMethod::Get, crow::HTTPMethod::Delete);

	methods.prefix("/cors").origin("same-origin");
	methods.prefix("/nocors").ignore();
}

IDevice* HttpServer::getDevice()
{
	QMutexLocker l(&mutex);
	return device;
}

void HttpServer::setLogLevel(crow::LogLevel level)
{
	crowApp.loglevel(level);
}

QString HttpServer::getServerName()
{
	constexpr int VER_MAJOR = 1;
	constexpr int VER_MINOR = 0;
	constexpr int VER_PATCH = 0;
	return QString::asprintf("npp-monitor http server V %d.%d.%d", VER_MAJOR, VER_MINOR, VER_PATCH);
}

uint16_t HttpServer::getServerPort()
{
	return crowApp.port();
}

void HttpServer::setServerPort(uint16_t port)
{
	crowApp.port(port);
}

void HttpServer::appRun()
{
	crowApp.signal_clear();
	crowApp.run();
}

void HttpServer::start()
{
#if QT_VERSION < QT_VERSION_CHECK(6,0,0)
	crowFuture = QtConcurrent::run(this, &HttpServer::appRun);
#else
	appFuture = QtConcurrent::run(&HttpServer::appRun, this);
#endif
}

void HttpServer::stop()
{
	qInfo().noquote() << "Stop the " << getServerName();
	crowFuture.cancel();
	QThread::msleep(500);
	crowApp.stop();
	//crowFuture.waitForFinished();
}

void HttpServer::initHandlers()
{
	namespace  pl = std::placeholders;

	CROW_ROUTE(crowApp, http_api::PATH_DEVICE).methods(crow::HTTPMethod::Get)
	(t_handler(std::bind(&HttpServer::deviceInfo, this, pl::_1, pl::_2)));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION).methods(crow::HTTPMethod::Post)
	(t_handler(std::bind(&HttpServer::sessionCreate, this, pl::_1, pl::_2)));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION_ACTIONS).methods(crow::HTTPMethod::Delete)
	(t_handler_s(std::bind(&HttpServer::sessionDelete, this, pl::_1, pl::_2, pl::_3)));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION_ACTIONS).methods(crow::HTTPMethod::Put)
	(t_handler_s(std::bind(&HttpServer::sessionPut, this, pl::_1, pl::_2, pl::_3)));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION_DEVICE_SIGNAL).methods(crow::HTTPMethod::Post)
	(t_handler_s(std::bind(&HttpServer::deviceSignalActions, this, pl::_1, pl::_2, pl::_3)));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION_DEVICE_SIGNAL).methods(crow::HTTPMethod::Get)
	(t_handler_s(std::bind(&HttpServer::deviceSignalActions, this, pl::_1, pl::_2, pl::_3)));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION_DEVICE_SIGNAL).methods(crow::HTTPMethod::Delete)
	(t_handler_s(std::bind(&HttpServer::deviceSignalActions, this, pl::_1, pl::_2, pl::_3)));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION_DEVICE_NORMALIZATION).methods(crow::HTTPMethod::Post)
	(t_handler_s(std::bind(&HttpServer::startNormalization, this, pl::_1, pl::_2, pl::_3)));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION_DEVICE_BROKENCHANNELS).methods(crow::HTTPMethod::Get)
	(t_handler_s(std::bind(&HttpServer::getBrokenChannels, this, pl::_1, pl::_2, pl::_3)));
}

void HttpServer::initOptionsHandlers()
{
	namespace  pl = std::placeholders;
	auto optsHandler = std::bind(&HttpServer::optionsHandler, this, pl::_1, pl::_2, "");
	auto optsHandlerS = std::bind(&HttpServer::optionsHandler, this, pl::_1, pl::_2, pl::_3);

	CROW_ROUTE(crowApp, http_api::PATH_DEVICE).methods(crow::HTTPMethod::Options)
	(t_handler(optsHandler));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION).methods(crow::HTTPMethod::Options)
	(t_handler(optsHandler));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION_ACTIONS).methods(crow::HTTPMethod::Options)
	(t_handler_s(optsHandlerS));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION_DEVICE_SIGNAL).methods(crow::HTTPMethod::Options)
	(t_handler_s(optsHandlerS));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION_DEVICE_NORMALIZATION).methods(crow::HTTPMethod::Options)
	(t_handler_s(optsHandlerS));

	CROW_ROUTE(crowApp, http_api::PATH_SESSION_DEVICE_BROKENCHANNELS).methods(crow::HTTPMethod::Options)
	(t_handler_s(optsHandlerS));
}

void HttpServer::invalidRequestHandler(const crow::request& req, crow::response& resp)
{
	qWarning().noquote() << QString::asprintf
						 (
							 "Unknown %s from %s. URL: %s"
							 , crow::method_name(req.method).data()
							 , req.remote_ip_address.data()
							 , req.raw_url.data()
						 );

	resp.code = crow::status::BAD_REQUEST;
	resp.end();
}

void HttpServer::optionsHandler(const crow::request& req, crow::response& resp, const std::string& str )
{
	Q_UNUSED(req)
	Q_UNUSED(str);

	resp.add_header("Access-Control-Allow-Headers", "content-type");
	resp.add_header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, x_remote_user, Content-Type, Accept, Kpi-Available-Resource-Id, Kpi-Complex-Resource-Id, Kpi-Contract-Id, Kpi-Lpu-Id, Kpi-Role-Id, Kpi-User-Token, Kpi-User-Rights");
	resp.add_header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, HEAD, OPTIONS");
	resp.add_header("Access-Control-Allow-Origin", "*");
	resp.add_header("Content-type", "text/json");
	resp.code = crow::status::OK;
	return resp.end();
}

HttpServer::RespondCodes HttpServer::_isDevicePresent() const noexcept
{
	return (device) ? RespondCodes::OK : RespondCodes::DeviceNotConnect ;
}

HttpServer::RespondCodes HttpServer::_checkSession(const QString& sessId ) const noexcept
{
	if (sessId.isEmpty() )
	{
		return  RespondCodes::InvalidParameter;
	}

	if (!isSameSession(sessId) )
	{
		return RespondCodes::SessionNotFound;
	}

	extendSession();
	return _isDevicePresent();
}

void HttpServer::respondJson(crow::response& resp, const QJsonObject& json, crow::status code)
{
	resp.code = code;
	QByteArray data = QJsonDocument(json).toJson(QJsonDocument::JsonFormat::Compact);
	resp.end(data.constData());
}

QJsonObject HttpServer::requestJson(const crow::request& req)
{
	QByteArray data (req.body.data());
	QJsonObject json = QJsonDocument::fromJson(data).object();
	return json;
}

void HttpServer::deviceInfo(const crow::request& req, crow::response& resp)  noexcept
{
	Q_UNUSED(req);

	DeviceFeatures features;
	{
		QMutexLocker l(&mutex);
		resp.code = int(_isDevicePresent());

		if (crow::status::OK != resp.code)
			return resp.end();

		features = device->features();
	}

	QVariantMap info;
	QJsonObject json;

	info[http_api::DEV_INVENTORY] = features.inventoryNum.toUtf8();
	info[http_api::DEV_MODEL] = features.model.toUtf8();
	info[http_api::DEV_VERSION] = features.version.toUtf8();
	info[http_api::DEV_DIGIT_RES] = features.resolution;
	info[http_api::DEV_SAMPLE_RATE] = features.sampleRate;
	json[http_api::DEV_FEATURES] = QJsonObject::fromVariantMap(info);
	return respondJson(resp, json);
}

void HttpServer::sessionCreate(const crow::request& req, crow::response& resp)
{
	QJsonObject rjson = requestJson (req);
	int duration = getIntegerValue(rjson["sessionLength"].toVariant());

	if (duration < 1 || rjson.count() > 1)
	{
		resp.code = int(RespondCodes::InvalidParameter);
		return resp.end();
	}

	qInfo().noquote() << "New session request. Duration" << duration << "s";
	QMutexLocker l(&mutex);

	RespondCodes rcode = _isDevicePresent();

	if (rcode == RespondCodes::OK && !session.isEmpty() )
	{
		qInfo().noquote() << "Rejected. Active session : " << this->session
						  << ", remain"
						  << sessionTimer.interval() / 1000 << "s";
		rcode = RespondCodes::DeviceBusy;
	}

	if (rcode != RespondCodes::OK)
	{
		resp.code = int(rcode);
		return resp.end();
	}

	QString version;
	createSession(duration, version);
	extendSession();
	QString expireTs = _sessionExpiredTimestamp();

	qInfo().noquote() << "Create session " << session
					  << ", duration " << duration << " secs"
					  << ", expired at " << expireTs;

	QVariantMap sessionData;

	sessionData[http_api::SESSION_ID] = session;
	sessionData[http_api::SESSION_EXPIRY] = expireTs;

	QJsonObject json;
	json[http_api::SESSION] = QJsonObject::fromVariantMap(sessionData);
	respondJson(resp, json);
}

void HttpServer::sessionExpired()
{
	qInfo().noquote() << "Session " << session << " expired";
	QMutexLocker l(&mutex);
	_sessionStop();
}

bool HttpServer::getLogReadInterval() const
{
	return logReadInterval;
}

void HttpServer::setLogReadInterval(bool newLogReadInterval)
{
	logReadInterval = newLogReadInterval;
}

void HttpServer::_sessionStop()
{
	stopTimer(&sessionTimer);
	if (_isSessionActive())
	{
		qInfo().noquote() << "Close session " << session;
		session.clear();
	}

	if (device)
		device->stop();
}

/**
 * @brief HttpServer::sessionDelete
 * @param req  - request
 * @param resp - repond
 * @param param - sessionId as std::string
 *
 * request to stop session
 */

void HttpServer::sessionDelete(const crow::request& req, crow::response& resp, const std::string& ssId)
{
	Q_UNUSED(req);
	QString sid = QString::fromStdString(ssId);

	QMutexLocker l(&mutex);

	if ((resp.code = int(_checkSession(sid))) == crow::status::OK)
	{
		_sessionStop();
	}
	resp.end();
}

void HttpServer::sessionPut(const crow::request& req, crow::response& resp, const std::string& ssId)
{
	Q_UNUSED(req);
	if (requestJson(req).count())
	{
		resp.code = int(RespondCodes::InvalidParameter);
	}
	else
	{
		QString sid = QString::fromStdString(ssId);
		QMutexLocker l(&mutex);
		resp.code = int(_checkSession(sid));
	}
	resp.end();
}

void HttpServer::startNormalization(const crow::request& req, crow::response& resp, const std::string& ssId)
{
	QJsonObject json = requestJson(req);
	int duration = getIntegerValue(json[http_api::NORMALIZATION_INTERVAL].toVariant());
	if (duration < 1 || json.count() > 1)
	{
		resp.code = int(RespondCodes::InvalidParameter);
		return resp.end();
	}

	QString sid = QString::fromStdString(ssId);
	QMutexLocker l(&mutex);

	if ((resp.code = int(_checkSession(sid))) == int(RespondCodes::OK))
	{
		device->normalization(duration);
	}

	resp.end();
}

QString HttpServer::brokenChannelsText(quint16 state)
{
	QStringList lst;
	QMutexLocker l(&mutex);
	for (size_t i = 0; i < device->channelsCount(); i++)
	{
		if (state & 0x01)
		{
			lst.append(QString::number(i));
		}
		state >>= 1;
	}

	QString channelText = lst.join(http_api::BROKEN_CHANNELS_SEPARATOR);
	return channelText;
}

void HttpServer::getBrokenChannels(const crow::request& req, crow::response& resp, const std::string& ssId)
{
	qInfo() << "Get broken channel";

	if (requestJson(req).count())
	{
		resp.code = int(RespondCodes::InvalidParameter);
		return resp.end();
	}

	QString sid = QString::fromStdString(ssId);
	uint16_t channelsMask = -1;

	{
		QMutexLocker l(&mutex);

		if ((resp.code = int(_checkSession(sid))) != int(RespondCodes::OK))
			return resp.end();

		if (!device->isActive())
		{
			resp.code = crow::status(RespondCodes::DeviceNotConnect);
			return resp.end();
		}

		channelsMask = device->brokenChannels();
	}

	QJsonObject obj;
	obj[http_api::BROKEN_CHANNELS] = brokenChannelsText( channelsMask );
	respondJson(resp, obj);
}

void HttpServer::deviceSignalActions(const crow::request& req, crow::response& resp, const std::string& ssId)
{
	QMutexLocker l(&mutex);
	resp.code = int(_isDevicePresent());

	if (resp.code != crow::status::OK)
		return resp.end();

	QString sid = QString::fromStdString(ssId);

	if ((resp.code = int(_checkSession(sid))) == int(RespondCodes::OK))
	{
		using namespace  crow;
		QJsonObject json = requestJson(req);
		switch (req.method)
		{
			case HTTPMethod::POST :
				return deviceReadSignalStart(json, resp);

			case HTTPMethod::DELETE :
				return deviceReadSignalStop(json, resp);

			case HTTPMethod::GET :
				return deviceReadSignalData(json, resp);

			default :
				resp.code = BAD_REQUEST;
				break;
		}
	}
	resp.end();
}

std::tuple<int, int, int> HttpServer::checkReadSignalStartParams(const QJsonObject& reqData)
{
	int code = int(RespondCodes::OK);
	int resolution = getIntegerValue(reqData[http_api::SIGNAL_READ_RESOLUTION].toVariant());
	int sampleRate = getIntegerValue(reqData[http_api::SIGNAL_READ_SAMPLE_RATE].toVariant());

	if (reqData.count() > 2)
	{
		qWarning() << "Request extra parameters";
		code = int(RespondCodes::InvalidParameter);
	}

	if (!enabledResolution.contains(resolution))
	{
		qWarning() << "Resolution " << resolution << "is not allowed";
		code = int(RespondCodes::InvalidParameter);

	}

	if (!enabledSampleRate.contains(sampleRate))
	{
		qWarning() << "Sample rate " << sampleRate << "is not allowed";
		code = int(RespondCodes::InvalidParameter);
	}

	return std::make_tuple(code, resolution, sampleRate);

}

void HttpServer::deviceReadSignalStart(const QJsonObject& reqData, crow::response& resp) noexcept
{
	int resolution;
	int sampleRate;
	std::tie(resp.code, resolution, sampleRate) = checkReadSignalStartParams(reqData);
	if (resp.code == int(RespondCodes::OK))
	{
		resampler.setOutputParameters(sampleRate, resolution);
		qInfo() << "Start signal read";
		qInfo().noquote() << QString::asprintf("Input  samples : %lu, resolution : %02lf", resampler.inputSamples(), resampler.inputResolution()) ;
		qInfo().noquote() << QString::asprintf("Output samples : %lu, resolution : %02lf", size_t(sampleRate), double(resolution) );

		resp.code = int(device->start() ? RespondCodes::OK : RespondCodes::DeviceNotConnect);
		if (resp.code != int(RespondCodes::OK))
		{
			qWarning() << "Start signal read error";
		}
	}
	resp.end();
}

void HttpServer::deviceReadSignalStop(const QJsonObject& reqData, crow::response& resp) noexcept
{
	if ( reqData.count() )
	{
		resp.code = int(RespondCodes::InvalidParameter);
		qWarning() << "Request extra parameters";
		return resp.end();
	}

	readElapsed.invalidate();
	qInfo() << "Stop signal read";
	device->stop();
	resp.code = crow::status::OK;
	resp.end();
}

void HttpServer::placeChannelData(QByteArray& destData, IDevice::const_pointer_v src_beg, size_t channel, size_t chCount, size_t readSize)
{

	IDevice::const_pointer_v src_end = src_beg + readSize;

	IDevice::pointer_v dest = reinterpret_cast<IDevice::pointer_v>(destData.data()) + channel;
	while (src_beg < src_end)
	{
		int16_t value = *src_beg ;
		*dest = qToLittleEndian( value );
		++src_beg;
		dest += chCount;
	}
}

size_t  HttpServer::prepareSignalData(QByteArray& result, size_t& readSize)
{
	size_t channelCount = device->channelsCount();

	Resampler::Values channelData(readSize);
	Resampler::Values outputData;

	for (size_t ch = 0; ch < channelCount;  )
	{
		device->readSignals(ch, channelData.data(), readSize);
		resampler.resample(channelData, outputData);
		if (result.isEmpty())
		{
			result.resize(sizeof(IDevice::value_type)*outputData.size()*channelCount);
		}
		placeChannelData(result, outputData.data(), ch, channelCount, outputData.size());
		++ch;
	}


	readSize -= resampler.needToResample();
	device->removeSignals(readSize  );
	return outputData.size();
}

size_t HttpServer::getSignalSamplesCount()
{
	size_t samplesCount = device->samplesCount();
	QElapsedTimer deadLineTimer;
	deadLineTimer.start();

	while (samplesCount < resampler.getInputThreshold() && deadLineTimer.hasExpired(500))
	{
		QThread::msleep(10);
		samplesCount = device->samplesCount();
	}

	return samplesCount;
}

void HttpServer::checkReadInterval(size_t readSize, size_t outputSize, int samplesBytes)
{
	if (!getLogReadInterval())
		return;

	QString text ;
	if (readElapsed.isValid())
	{
		text = QString("Request interval is %1").arg(readElapsed.elapsed());
		readElapsed.restart();
	}
	else
	{
		text = "It is first read request";
		readElapsed.start();
	}

	text += QString(", accumulated samples %1").arg(readSize);
	if (readSize != outputSize)
		text += QString(" resampled to %1 ").arg( outputSize);

	text += QString(", bytes %1").arg(samplesBytes);

	qInfo().noquote() << text;
}

void HttpServer::deviceReadSignalData(const QJsonObject& reqData, crow::response& resp) noexcept
{
	if (reqData.count())
	{
		resp.code = int(RespondCodes::InvalidParameter);
		return resp.end();
	}

	if (!device->isActive())
	{
		resp.code = int(RespondCodes::DeviceNotConnect);
		return resp.end();
	}

	resp.code = crow::status::OK;

	size_t readSamples = getSignalSamplesCount();
	size_t outputSamples = readSamples;

	crow::status status = crow::status(device->isActive() ? RespondCodes::OK : RespondCodes::DeviceNotConnect );
	QJsonObject json;
	json[http_api::BROKEN_CHANNELS] = brokenChannelsText( device->brokenChannels() );

	QByteArray samples;

	if (readSamples)
	{
		int missedCount = device->missedCount();
		if (missedCount)
		{
			qWarning() << "Missed " << missedCount << " samples ";
		}
		json[http_api::SIGNAL_READ_MISSED_SIZE] = missedCount;

		outputSamples = prepareSignalData(samples, readSamples);
		samples = samples.toBase64();
		json[http_api::SIGNAL_READ_DATA] = samples.data();
	}

	json[http_api::SIGNAL_READ_SIZE] = int(outputSamples);

	checkReadInterval(readSamples, outputSamples, samples.size());

	return respondJson(resp, json, status);
}

void HttpServer::createSession(int secsDuration, const QString& ver)
{
	Q_UNUSED(ver)

	namespace chr = std::chrono ;
	int ms = chr::duration_cast<chr::milliseconds>(chr::seconds(secsDuration)).count();
	sessionTimer.setInterval(ms);

	QUuid uid = QUuid::createUuid();
	session = QString::asprintf
			  (
				  "%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X",
				  uid.data1, uint(uid.data2), uint(uid.data3),
				  uint(uid.data4[0]), uint(uid.data4[1]), uint(uid.data4[2]), uint(uid.data4[3]),
				  uint(uid.data4[4]), uint(uid.data4[5]), uint(uid.data4[6]), uint(uid.data4[7])
			  );
}

QString HttpServer::_sessionExpiredTimestamp() const
{
	namespace chr = std::chrono ;

	QDateTime dtm = QDateTime::currentDateTime();
	QTimeZone tz = dtm.timeZone();
	dtm = dtm.addSecs(chr::duration_cast<chr::seconds>(chr::milliseconds(sessionTimer.interval())).count());
	QString ret = dtm.toString(Qt::DateFormat::ISODate);
	return ret;
}

bool HttpServer::checkApiVersion(const crow::request& req)
{
	if (req.method == crow::HTTPMethod::Options)
		return true;

	bool eq {false};
	auto ptr = req.headers.find(http_api::API_VER_KEY);
	if (ptr != req.headers.end())
	{
		std::string ver(http_api::API_VER_1_0_0);
		eq =  ver == std::string(ptr->second.data());
	}

	if (!eq)
	{
		qWarning() << "Request : " << req.raw_url.data() << "do not contains protocol version or version incorrect";

	}
	return eq;
}

bool HttpServer::_isSessionActive() const
{
	return !session.isEmpty();
}

bool HttpServer::isSameSession(const QString& ssId) const
{
	return 0 == session.compare(ssId, Qt::CaseSensitivity::CaseInsensitive);
}

void HttpServer::extendSession() const
{
	QTimer* timer = const_cast<QTimer*>(&sessionTimer);
	stopTimer(timer);
	startTimer(timer);
}

void HttpServer::stopTimer(QTimer* timer)
{

	QMetaObject::invokeMethod(timer, "stop", Qt::ConnectionType::QueuedConnection);
}

void HttpServer::startTimer(QTimer* timer)
{
	QMetaObject::invokeMethod(timer, "start", Qt::ConnectionType::QueuedConnection);
}


