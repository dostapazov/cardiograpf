﻿#ifndef HTTPSERVER_HPP
#define HTTPSERVER_HPP

#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QScopedPointer>
#include <QFuture>
#include <QMutex>
#include <QTimer>
#include <QList>
#include <QElapsedTimer>

#include <crow/app.h>
#include <crow/http_request.h>
#include <crow/http_response.h>
#include <crow/middlewares/cors.h>
#include <devices/idevice.hpp>
#include <resampler/resampler.hpp>

class HttpServer : public QObject
{
	Q_OBJECT
	enum class RespondCodes : int
	{
		OK = crow::status::OK,
		UnknownError = crow::status::BAD_REQUEST,
		InvalidParameter = crow::status::BAD_REQUEST,
		SessionNotFound = crow::status::NOT_FOUND,
		DeviceNotConnect = crow::status::BAD_GATEWAY,
		DeviceBusy = crow::status::SERVICE_UNAVAILABLE,
		DeviceConnectTimeout = crow::status::GATEWAY_TIMEOUT
	};
	explicit HttpServer(QObject* parent = nullptr);

public:
	static HttpServer& instance();
	uint16_t getServerPort();
	void     setServerPort(uint16_t port);
	IDevice* getDevice();
	void     setLogLevel(crow::LogLevel level);

	bool getLogReadInterval() const;
	void setLogReadInterval(bool newLogReadInterval);

Q_SIGNALS:

public Q_SLOTS:
	void setDevice(IDevice* dev);
	void start();
	void stop();

private Q_SLOTS:
	void sessionExpired();

private:
	class HeadersMiddleware
	{
	public:
		struct context {};
		void before_handle(crow::request& req, crow::response& res, context& ctx);
		void after_handle(crow::request& req, crow::response& res, context& ctx);
		HttpServer* owner = nullptr;
	};

	using CrowApp = crow::App<crow::CORSHandler, HeadersMiddleware>;
	CrowApp crowApp;
	QFuture<void> crowFuture;

	IDevice*  device = nullptr;
	QTimer    sessionTimer;
	QString   session;
	bool      logReadInterval = false;

	Resampler resampler;
	QElapsedTimer readElapsed;
	mutable QMutex mutex;

	static QList<int> enabledResolution ;
	static QList<int> enabledSampleRate ;

private:
	void appRun();

	using t_handler    = std::function<void(const crow::request& req, crow::response& resp)>;
	using t_handler_s  = std::function<void(const crow::request& req, crow::response& resp, const std::string&)>;
	using t_handler_ss = std::function<void(const crow::request& req, crow::response& resp, const std::string&, const std::string&)>;

	void    initCorsRules();
	void    initHandlers();
	void    initOptionsHandlers();

	void    deviceInfo(const crow::request& req, crow::response& resp ) noexcept;
	void    sessionCreate(const crow::request& req, crow::response& resp);
	void    sessionDelete(const crow::request& req, crow::response& resp, const std::string& ssId);
	void    sessionPut(const crow::request& req, crow::response& resp, const std::string& ssId);
	void    deviceSignalActions(const crow::request& req, crow::response& resp, const std::string& ssId);
	void    startNormalization(const crow::request& req, crow::response& resp, const std::string& ssId);
	void    getBrokenChannels(const crow::request& req, crow::response& resp, const std::string& ssId);
	QString brokenChannelsText(quint16 state);

	void    deviceReadSignalStart(const QJsonObject& reqData, crow::response& resp) noexcept;
	void    deviceReadSignalStop(const QJsonObject& reqData, crow::response& resp) noexcept;
	void    deviceReadSignalData(const QJsonObject& reqData, crow::response& resp) noexcept;
	void    optionsHandler(const crow::request& req, crow::response& resp, const std::string& str);

	static
	void    invalidRequestHandler(const crow::request& req, crow::response& resp);

	static QString getServerName();
	RespondCodes   _checkSession(const QString& sessId) const noexcept;
	RespondCodes   _isDevicePresent() const noexcept;

	static void respondJson(crow::response& resp, const QJsonObject& json, crow::status code = crow::status::OK);
	static QJsonObject requestJson(const crow::request& req);

	size_t prepareSignalData(QByteArray& result, size_t& samplesCount);
	void placeChannelData(QByteArray& destData, const IDevice::const_pointer_v, size_t channel, size_t chCount, size_t readSize);

	void createSession(int secsDuration, const QString& ver);
	bool isSameSession(const QString& ssId) const;
	void extendSession() const;
	void _sessionStop();
	bool _isSessionActive() const;
	QString _sessionExpiredTimestamp() const;
	bool checkApiVersion(const crow::request& req);
	size_t getSignalSamplesCount();
	void checkReadInterval(size_t readSize, size_t outputSize, int samplesBytes);
	std::tuple<int, int, int> checkReadSignalStartParams(const QJsonObject& reqData);
	static void stopTimer(QTimer* timer);
	static void startTimer(QTimer* timer);
};

#endif // HTTPSERVER_HPP
