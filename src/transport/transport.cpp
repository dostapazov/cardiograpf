﻿#include "transport.hpp"
#include <QDebug>
#include <chrono>

Transport::Transport(QObject* parent)
	: QObject{parent}
{
	readTimer.setInterval(250);
	connect(&readTimer, &QTimer::timeout, this, &Transport::onReadyRead);
}

void Transport::setConfig(const Config& cfg)
{
	m_config = cfg;
	reset();
}

bool Transport::start()
{
	if (!isConfigured())
		return false;

	reset();
	m_receiveState = ReceiveState::WaitStart;
	return true;
}

void Transport::stop()
{
	m_receiveState = ReceiveState::Inactive;
}

void Transport::detachIo()
{
	QSignalBlocker sb(readTimer);
	if (m_io)
	{
		readTimer.stop();
		m_io->disconnect(this);
	}
}

void Transport::attachIo()
{
	if (m_io)
	{
		QObject::connect(m_io, &QIODevice::readyRead, this, &Transport::onReadyRead);
		QObject::connect(m_io, &QIODevice::destroyed, this, &Transport::ioDestroyed);
	}
}

void Transport::setIoDevice(QIODevice* io)
{
	if (io == m_io)
		return;

	detachIo();
	m_io = io;
	attachIo();
	reset();
}

void Transport::ioDestroyed(QObject* ptr)
{
	if (ptr == m_io)
	{
		m_io = nullptr;
	}
}

void Transport::reset()
{
	m_buffer.reserve(m_config.payloadSize);
	m_bytesRecved = m_framesRecved = m_framesLost = m_framesPerSecond = framesCounter = 0;
	elapsed.invalidate();
	elapsed.start();
	m_buffer.clear();
}

int Transport::currentPayloadSize()
{
	return m_buffer.size();
}

void Transport::onReadyRead()
{
	if (sender() == &readTimer)
	{
		qWarning() << "Transport: read by tymer";
	}
	readTimer.stop();
	char buffer[2048];
	size_t count;
	while ((count = m_io->read(buffer, sizeof(buffer))))
	{
		processReceiveRaw(buffer, count);
	}
	readTimer.start();
}

Transport::ReceiveState Transport::recvStart
(
	QByteArray::const_iterator& beg,
	QByteArray::const_iterator end
)
{
	beg = std::find(beg, end, QByteArray::value_type(m_config.start));
	if (beg == end)
	{
		beg = end;
		return ReceiveState::WaitStart;
	}

	m_buffer.clear();
	++beg;
	return ReceiveState::ReceivePayload;
}

Transport::ReceiveState Transport::resyncRecv(QByteArray::const_iterator& beg, QByteArray::const_iterator end)
{
	QByteArray::const_iterator ptr = std::find(m_buffer.constBegin(), m_buffer.constEnd(), QByteArray::value_type(m_config.start));
	if (ptr < m_buffer.constEnd())
	{
		QByteArray::difference_type offset = std::distance(m_buffer.constBegin(), ptr);
		m_buffer.remove(0, ++offset);
		return recvPayload(beg, end);
	}

	m_buffer.clear();
	m_receiveState = ReceiveState::WaitStart;
	return recvStart(beg, end);
}

void Transport::updateFramesPerSecond()
{
	constexpr uint64_t MSECS = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::seconds(1)).count();
	if (elapsed.hasExpired( MSECS) || !elapsed.isValid())
	{
		if (m_framesPerSecond != framesCounter)
		{
			emit fpsChanged(framesCounter, QPrivateSignal());
		}

		m_framesPerSecond = framesCounter;
		framesCounter = 0;
		elapsed.restart();
		return;
	}
	++framesCounter;
}

Transport::ReceiveState Transport::recvStop
(
	QByteArray::const_iterator& beg, QByteArray::const_iterator end
)
{
	if (uint8_t(*beg) == m_config.stop)
	{
		++m_framesRecved;
		++beg;
		updateFramesPerSecond();
		emit frameRecved(m_buffer, QPrivateSignal());
		m_buffer.clear();
		return ReceiveState::WaitStart;
	}

	if (m_framesRecved)
	{
		++m_framesLost;
		qInfo() << "Lost frame, total :" << m_framesLost << " debug timer elapsed " << debugTimer.elapsed() ;
	}

	return resyncRecv(beg, end);
}

Transport::ReceiveState Transport::recvPayload
(
	QByteArray::const_iterator& beg,
	QByteArray::const_iterator end
)
{
	size_t remain = m_config.payloadSize - m_buffer.size();
	size_t inSize = std::distance(beg, end);
	size_t sizeToAppend = std::min(remain, inSize);

	m_buffer.append(beg, sizeToAppend);
	beg += sizeToAppend;

	if (m_buffer.size() == m_config.payloadSize)
		return ReceiveState::WaitStop ;

	return ReceiveState::ReceivePayload;
}

void Transport::processReceive(const QByteArray& data)
{
	processReceiveRaw(data.constBegin(), data.size());
}

void Transport::processReceiveRaw(const char* begin, size_t count)
{
	if (!m_config.payloadSize)
		return;

	debugTimer.restart();
	m_bytesRecved += count;

	QByteArray::const_iterator it_beg = begin;
	QByteArray::const_iterator it_end = begin + count;

	while (it_beg < it_end)
	{
		switch (m_receiveState)
		{
			case ReceiveState::WaitStart :
				m_receiveState = recvStart(it_beg, it_end);
				break;
			case ReceiveState::ReceivePayload :
				m_receiveState = recvPayload(it_beg, it_end);
				break;
			case ReceiveState::WaitStop :
				m_receiveState = recvStop(it_beg, it_end);
				break;
			default :
				// Inactive state - ignore all
				m_bytesRecved = m_framesRecved = 0;
				return;
		}
	}
}
