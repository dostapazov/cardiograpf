﻿#ifndef TRANSPORT_HPP
#define TRANSPORT_HPP

#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QIODevice>
#include <QElapsedTimer>
#include <QTimer>

class Transport : public QObject
{
	Q_OBJECT
public:
	struct Config
	{
		Config() = default;
		Config(uint8_t aStart, uint8_t aStop, uint16_t pSz): start(aStart), stop(aStop), payloadSize(pSz) {}
		uint8_t  start = 0;
		uint8_t  stop = 0;
		uint16_t payloadSize = 0;
	};

	enum class ReceiveState
	{
		Inactive, WaitStart, ReceivePayload, WaitStop
	};

public:
	explicit Transport(QObject* parent = nullptr);
	const Config& config();
	void  setConfig(const Config& cfg);
	void  setIoDevice(QIODevice* io);
	bool  isConfigured();
	ReceiveState getReceiveState();
	int currentPayloadSize();
	uint64_t recvedBytes();
	uint64_t recvedFrames();
	uint64_t lostFrames();

Q_SIGNALS:
	void frameRecved(const QByteArray& frame, QPrivateSignal);
	void fpsChanged(int fps, QPrivateSignal);

protected:
	void processReceive(const QByteArray& data);
	void processReceiveRaw(const char* begin, size_t count);

public Q_SLOTS:
	bool start();
	void stop();

private Q_SLOTS:
	void onReadyRead();
	void ioDestroyed(QObject* ptr);

private:
	Config m_config;
	uint64_t   m_bytesRecved = 0;
	uint64_t   m_framesRecved = 0;
	uint64_t   m_framesLost = 0;

	QIODevice* m_io = nullptr;
	QByteArray m_buffer;
	ReceiveState m_receiveState = ReceiveState::Inactive;
	int      m_framesPerSecond = 0;
	int      framesCounter = 0;
	QElapsedTimer elapsed;
	QElapsedTimer debugTimer;
	QTimer        readTimer;
private:
	void reset();
	Transport::ReceiveState recvStart(QByteArray::const_iterator& beg, QByteArray::const_iterator end);
	Transport::ReceiveState recvStop(QByteArray::const_iterator& beg, QByteArray::const_iterator end);
	Transport::ReceiveState recvPayload(QByteArray::const_iterator& beg, QByteArray::const_iterator end);
	Transport::ReceiveState resyncRecv(QByteArray::const_iterator& beg, QByteArray::const_iterator end);
	void updateFramesPerSecond();
	void detachIo();
	void attachIo();
};

inline uint64_t Transport::recvedBytes()
{
	return m_bytesRecved;
}
inline uint64_t Transport::recvedFrames()
{
	return m_framesRecved;
}

inline uint64_t Transport::lostFrames()
{
	return m_framesLost;
}

inline bool  Transport::isConfigured()
{
	return m_config.payloadSize;
}

inline Transport::ReceiveState Transport::getReceiveState()
{
	return m_receiveState;
}

inline const Transport::Config& Transport::config()
{
	return m_config;
}

#endif // TRANSPORT_HPP
